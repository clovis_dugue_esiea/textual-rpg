=== PREAMBULE ===

Dans ce repertoire se trouve le projet Visual Studio 2017 de 3AS1, realise par
Clovis DUGUE et Raphael MEIRA RODRIGUES.


=== DETAILS DES FICHIERS ===

Les fichiers .h sont tous commentes selon la norme Doxygen. Le Doxyfile est
disponible a la racine de ce repertoire. La documentation, deja generee, se
trouve dans le sous-dossier "DOC/"", sous les formats XML, RTF et HTML.

Dans le sous-dossier "DUGUE_MEIRA/"" se trouve le code source.
Chaque classe a ses deux fichiers "Nom.h" et "Nom.cpp", sauf "Game" qui est
decomposee en deux fichiers ".cpp" : "GameInit" et "GamePlay"

Les fichiers "utils.cpp", "utils.h", "ConsoleColor.h" et "main.cpp" definissent
differentes structures, macro, et inclusions utiles a la compilation du projet

L'executable du jeu, deja compile, se trouve dans le sous-dossier "x64/Debug/".


=== DEROULEMENT DU PROGRAMME ===

Le programme se deroule en de multiples phases

Au moment du lancement, l'utilisateur a le choix entre un mode campagne (1) et un
mode r�seau pour jouer avec deux ordinateurs (2)
S'il choisit le mode campagne, le jeu se lance, et l'utilisateur controlera les
deux equipes
S'il choisit le mode r�seau, on lui demandera s'il souhaite heberger ou
rejoindre une partie

A des fins de tests, on pourra utiliser deux ordinateurs, ou bien lancer
l'executable 2 fois sur le meme PC
Dans les deux cas, on peut utiliser l'IP LAN de la machine serveur pour se
connecter. Si les deux instances sont sur la meme machine, on pourra utilsier
l'IP "127.0.0.1"
Le port par defaut de connexion est le "53530"

Une fois les deux joueurs connectes, le jeu se deroulera de la meme facon qu'en
solo, hormis que l'on ne controle qu'un seul joueur. A la fin de chaque phase,
les donnees de la phase sont echangees entre la machine hote et la machine
client. Cela signifie que les deux machines doivent imperativement avoir le
meme sous-dossier "resources/" dans le meme dossier que l'executable, afin de ne
pas causer d'incoherence de communication

Une fois le jeu lance, les donnees des fichers de configuration ("resources/")
sont chargees

Puis, il est demande aux deux joueurs de choisir leurs personnages, puis
d'acheter leurs equipements et objets, et enfin d'equiper leurs armes

Dans ces phases de choix, rentrer le mot-cle "fuck" permet d'annuler la derniere
action effectuee. Toutefois, en mode multijoueur, l'utilisation de ce mot-cle
pour retourner en arriere peut causer des incoherence

Le mot-cle "done" est utilise pour indiquer le passage a une prochaine phase.
Par exemple, le joueur le tape quand il a termine ses achats dans le magasin,
pour passer a l'etape suivante

Une fois ces trois phases realisees, le jeu se lance. Plus d'annulation possible
chaque action est definitive.

Chaque personnage a acces a son attaque (armee ou non armee), son sac et ses
competences. La partie se termine quand les 3 joueurs d'une equipe sont morts
