#pragma once

#include "utils.h"
#include "Player.h"

// WARRIOR
/**
 * \brief	Aptitude "Hurlement" du guerrier : Provoque les autres personnages
 * \param p		Joueur lancant l'aptitude
 * \param t		Joueur cibl�
 */
void warrior_Scream(Player* p, Player *t = nullptr);
/**
 * \brief	Aptitude "Hurlement" du guerrier : Provoque les autres personnages
 * \param p		Joueur lancant l'aptitude
 * \param t		Joueur cibl�
 */void warrior_Fix(Player* p, Player *t);
// ARCHER
/**
 * \brief	Aptitude "Visee" de l'archer : Augmente la dexterite jusqu'a la prochaine attaque
 * \param p		Joueur lancant l'aptitude
 * \param t		Joueur cibl�
 */void archer_Aim(Player* p, Player *t = nullptr);
 /**
  * \brief	Aptitude "Antidote" de l'archer : Soigne un allie du poison
  * \param p		Joueur lancant l'aptitude
  * \param t		Joueur cibl�
  */void archer_Antidote(Player* p, Player *t);
// WIZARD
/**
 * \brief	Aptitude "Soin" du magicien : Restaure un nombre de points de vie a un allie
 * \param p		Joueur lancant l'aptitude
 * \param t		Joueur cibl�
 */void wizard_Heal(Player* p, Player *t);
/**
 * \brief	Aptitude "RegenMana" du magicien : Restaure un nombre de points de mana au lanceur
 * \param p		Joueur lancant l'aptitude
 * \param t		Joueur cibl�
 */void wizard_RegenMana(Player* p, Player *t = nullptr);
/**
 * \brief	Aptitude "Enchantement" du magicien : Augmente les degats de l'arme de melee de l'allie cible
 * \param p		Joueur lancant l'aptitude
 * \param t		Joueur cibl�
 */void wizard_Enchant(Player* p, Player *t);
// THIEF
/**
 * \brief	Aptitude "Invisibilite" du voleur : Rend le voleur inciblable pour un nombre de tours
 * \param p		Joueur lancant l'aptitude
 * \param t		Joueur cibl�
 */void thief_Invisibility(Player* p, Player *t = nullptr);