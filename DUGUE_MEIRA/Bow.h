#pragma once
#include "Ranged.h"

/**
 * \brief	Methodes correspondant a l'arc
 */
class Bow : public Ranged
{
public:
	/**
	 * \brief	Constructeur
	 * \param name		Nom de l'arme
	 * \param stats		Statistiques bonus ajoutees a celles du joueur
	 * \param wstats	Statistiques de l'arme
	 */
	Bow(std::string name, int *stats, int *wstats);
	/**
	 * \brief	Destructeur
	 */
	~Bow() = default;

	/**
	 * \brief	Utilisation de l'arme (decrementation de la durabilite)
	 * \return	Degats de l'arme
	 */
	int use() override;

	/**
	 * \brief	Donne la liste des classes pouvant porter cette arme
	 * \return	Set de nom de classes pouvant s'equiper l'arme
	 */
	std::set<std::string>  getCompatibility() override;
};

