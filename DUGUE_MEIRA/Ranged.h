#pragma once
#include "Weapon.h"

/**
 * \brief	Methodes associees aux armes a distance
 */
class Ranged : public Weapon
{
public:
	/**
	 * \brief	Constructeur
	 * \param name		Nom de l'arme
	 * \param stats		Statistiques bonus ajoutees a celles du joueur
	 * \param wstats	Statistiques de l'arme
	 */
	Ranged(std::string name, int *stats, int *wstats);
	/**
	 * \brief	Destructeur
	 */
	virtual ~Ranged() = default;

	/**
	 * \brief	Utilisation de l'arme (overridee)
	 * \return	Degats de l'arme
	 */
	int use() override = 0;

	/**
	 * \brief	Donne la liste des classes pouvant porter cette arme (overridee)
	 * \return	Set de nom de classes pouvant s'equiper l'arme
	 */
	std::set<std::string>  getCompatibility() override = 0;
};

