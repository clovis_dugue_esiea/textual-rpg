#include "Weapon.h"
#include <utility>
#include "Staff.h"
#include "Sword.h"
#include "Dagger.h"
#include "Bow.h"

Weapon::Weapon(std::string name, const int *stats, const int *wstats) :
	m_name(std::move(name))
{
	for (int i(0); i < NB_STATS; ++i) {
		m_stats[i] = stats[i];
	}
	for (int i(0); i < NB_STATSW; ++i) {
		m_wstats[i] = wstats[i];
	}
}

std::string Weapon::getName() { return m_name; }

int * Weapon::getStats() { return m_stats; }

int * Weapon::getStatsW() { return m_wstats; }