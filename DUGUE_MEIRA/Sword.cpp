#include "Sword.h"
#include <utility>
#include "Player.h"

Sword::Sword(std::string name, int *stats, int *wstats) :
	Melee(std::move(name), stats, wstats)
{
}

int Sword::use()
{
	// Declare variables for random number generation
	std::default_random_engine random_generator;
	const std::uniform_int_distribution<int> distribution(2, 5);

	// Compute wearing
	const int wearing = distribution(random_generator);

	// Decrease durability
	if (m_durability <= wearing) {
		m_durability = 0;
	}
	else {
		m_durability -= wearing;
	}

	// Return damage
	return m_stats[w_damage];
}

std::set<std::string> Sword::getCompatibility()
{
	std::set<std::string> v;
	v.insert("Warrior");
	v.insert("Wizard");
	return v;
}