#include "Game.h"

// Defines a struct
struct playing_character_position
{
	int player, character_id;
	playing_character_position(int player, int character_id) :
		player(player),
		character_id(character_id)
	{}
};

int Game::play()
{
	// Defines whether the came continues or not
	bool keep_playing = true;
	// Defines a queue for player at 0 speed
	std::vector<struct playing_character_position> character_queue;
	// Variables for random number generation
	std::default_random_engine random_generator;
	std::string s_err;
	int loser = -1;
	// Player targeted by... any action
	Player *target;
	bool taunting[3];
	// Multiplayer packet buffering
	std::string packet_buffer;

	// Main loop
	while (keep_playing) {
		// Reset loop variables
		unsigned char next_action = 0;
		// Check if any player can play
		for (int i = 0; i < 2; ++i) {
			int dead_counter = 0;
			for (int j = 0; j < 3; ++j) {
				// Forbid playing if the character is dead
				if (m_teams[i][j]->getLife() <= 0) {
					m_teams[i][j]->setSpeedFull();
					++dead_counter;
					// If every character is dead, exit
					if (dead_counter == 3) {
						keep_playing = false;
						loser = i;
					}
				}
				// Decrement speed
				else {
					m_teams[i][j]->decrementSpeed();
					// If some have a 0 speed, they are added to the playing queue
					if (m_teams[i][j]->getSpeed() <= 0) {
						// Decrement cooldowns
						for (auto& it : m_teams[i][j]->getSkills()) {
							if (it.second.m_cooldown > 0) {
								--it.second.m_cooldown;
							}
						}
						if (m_teams[i][j]->getStatus()[focus] > 0) {
							--m_teams[i][j]->getStatus()[focus];
						}
						// Reset speed
						m_teams[i][j]->setSpeedFull();
						// Handle poison
						if (m_teams[i][j]->getStatus()[poison] != 0) {
							m_teams[i][j]->setLife(m_teams[i][j]->getLife() * 11 / 12);
						}
						character_queue.emplace_back(i, j);
					}
				}
			}
		}
		if (!keep_playing) {
			break;
		}
		if (character_queue.empty()) {
			continue;
		}
		// Order randomly
		std::shuffle(character_queue.begin(), character_queue.end(), random_generator);
		// Make every character in the queue play
		while (!character_queue.empty()) {
			bool action_successful = false;
			bool jump = false;
			int taunting_counter = 0;
			for (bool& i : taunting) {
				i = false;
			}
			displayBoard(character_queue.back(), 0b00, s_err);
			// Display the board
			std::string user_input = "Fermer";
			while (user_input == "Fermer" || user_input == "Competences" || user_input == "Sac") {
				if (!m_multiplayer) {
					std::getline(std::cin, user_input);
				}
				else {
					user_input = mp_getInput(character_queue);
				}
				// Display the actions menu
				if (user_input == "Fermer") {
					next_action = 0b00;
					displayBoard(character_queue.back(), next_action);
				}
				// Display the skills menu
				else if (user_input == "Competences") {
					next_action = 0b01;
					displayBoard(character_queue.back(), next_action);
				}
				// Display the bag menu
				else if (user_input == "Sac") {
					next_action = 0b10;
					displayBoard(character_queue.back(), next_action);
				}
			}
			// Weapon / hand attack
			if (user_input == "Attaquer") {
				std::cout << "Quel personnage cibler ?" << std::endl;
				if (!m_multiplayer) {
					std::getline(std::cin, user_input);
				}
				else {
					user_input = mp_getInput(character_queue);
				}
				for (int i = 0; i < 3; ++i) {
					if (m_teams[(character_queue.back().player == 0 ? 1 : 0)][i]->getStatus()[focus] != 0) {
						++taunting_counter;
						taunting[i] = true;
					}
				}
				for (int i = 0; i < 3; ++i) {
					if (m_teams[(character_queue.back().player == 0 ? 1 : 0)][i]->getName() == user_input) {
						if (m_teams[(character_queue.back().player == 0 ? 1 : 0)][i]->getStatus()[invisibility] != 0) {
							s_err = "Le personnage n'est pas ciblable";
							jump = true;
							break;
						}
						if (taunting_counter != 0 && !taunting[i]) {
							s_err = "Un autre personnage vous provoque";
							jump = true;
							break;
						}
						target = m_teams[(character_queue.back().player == 0 ? 1 : 0)][i];
						if (target->getLife() <= 0) {
							s_err = "Cible deja morte";
							displayBoard(character_queue.back(), 0b00, s_err);
							break;
						}
						m_teams[character_queue.back().player][character_queue.back().character_id]->attack(target);
						action_successful = true;
						break;
					}
				}
				if (jump) {
					continue;
				}
				if (action_successful) {
					character_queue.pop_back();
					s_err = "";
					user_input = "Fermer";
					continue;
				}
				s_err = "Ce personnage n'est pas dans l'equipe adverse";
				displayBoard(character_queue.back(), 0b00, s_err);
				user_input = "Fermer";
				continue;
			}
			// Skip turn
			if (user_input == "Passer") {
				character_queue.pop_back();
				s_err = "";
				user_input = "Fermer";
				continue;
			}
			// If the action is a skill
			if (next_action == 0b01) {
				if (user_input == "Aim" && !(dynamic_cast<Archer *>(m_teams[character_queue.back().player][character_queue.back().character_id]))->m_bowequipped) {
					s_err = "Arc necessaire pour utiliser le sort de visee";
					user_input = "Fermer";
					continue;
				}
				if (m_teams[character_queue.back().player][character_queue.back().character_id]->getSkills().find(user_input) == m_teams[character_queue.back().player][character_queue.back().character_id]->getSkills().end()) {
					s_err = "Ce personnage n'a pas cette capacite";
					user_input = "Fermer";
					continue;
				}
				target = getSkillTarget(character_queue.back(), user_input);
				if (!target) {
					s_err = "La cible n'existe pas";
					user_input = "Fermer";
					continue;
				}
				if (target && target->getLife() <= 0) {
					s_err = "Cible deja morte";
					user_input = "Fermer";
					continue;
				}
				m_teams[character_queue.back().player][character_queue.back().character_id]->getSkills().at(user_input).m_skill(m_teams[character_queue.back().player][character_queue.back().character_id], target);
				character_queue.pop_back();
				s_err = "";
			}
			// If the action is an item
			if (next_action == 0b10) {
				target = getItemTarget(character_queue.back(), user_input);
				if (target && target->getLife() <= 0 && user_input != "Parchemin de resurrection") {
					s_err = "Cible deja morte";
					user_input = "Fermer";
					continue;
				}
				const int i_err = m_teams[character_queue.back().player][character_queue.back().character_id]->getBag()->use(
					user_input, target);
				switch (i_err) {
				case err_itemNotInBag:
					s_err = "Cet objet n'est pas dans le sac";
					user_input = "Fermer";
					continue;
				case err_itemNotImplemented:
					s_err = "Cet objet n'est pas utilisable";
					user_input = "Fermer";
					continue;
				case err_itemNeedsATarget:
					s_err = "Cible invalide";
					user_input = "Fermer";
					continue;
				case success:
					character_queue.pop_back();
					continue;
				default:
					break;
				}
			}
		}
	}

	// End the session, return the winner
	return loser + 1 % 2;
}

int Game::displayBoard(playing_character_position character, unsigned short mode, const std::string& err)
{
	auto skills = m_teams[character.player][character.character_id]->getSkills();
	const auto items = m_teams[character.player][character.character_id]->getBag()->getItems();

	// Clear screen
	system(clearcommand);
	std::cout << err << std::endl <<
		"\tNom\t\t\tVit.\tVie\tMana\t\tArme" <<
		std::endl;

	// Display complete board of characters
	for (int i = 0; i < 2; ++i) {
		std::cout <<
			"Joueur " <<
			i + 1 <<
			std::endl;
		for (int j = 0; j < 3; ++j) {
			const bool isWizard = m_teams[i][j]->getStats()[p_mana] > 0;
			if (m_teams[i][j]->getWeapon()) {
				std::cout <<
					(m_teams[i][j]->getStatus()[poison] != 0 ? "P" : " ") <<
					(m_teams[i][j]->getStatus()[focus] != 0 ? "F" : " ") <<
					(m_teams[i][j]->getStatus()[invisibility] != 0 ? "I" : " ") <<
					(m_teams[i][j]->getStatus()[aim] != 0 ? "A" : " ") <<
					(m_teams[i][j]->getStatus()[enchanted] != 0 ? "E" : " ") <<
					"\t" << m_teams[i][j]->getName() << (m_teams[i][j]->getName().length() >= 8 ? "" : "\t") << (m_teams[i][j]->getName().length() >= 16 ? "" : "\t") <<
					"\t" << (m_teams[i][j]->getSpeed() == m_teams[i][j]->getStats()[p_speed] ? 0 : m_teams[i][j]->getSpeed()) << "\t" << (m_teams[i][j]->getLife() < 0 ? 0 : m_teams[i][j]->getLife()) << "/" << m_teams[i][j]->getStats()[p_life] << "\t" << (isWizard ? std::to_string(m_teams[i][j]->getMana()) + "/" + std::to_string(m_teams[i][j]->getStats()[p_mana]) : "") <<
					"\t\t" << m_weapons.at(m_teams[i][j]->getWeapon()->getName())->m_type <<
					"\t" << m_teams[i][j]->getWeapon()->getName() <<
					(dynamic_cast<Melee *>(m_teams[i][j]->getWeapon()) ? "(" + std::to_string(dynamic_cast<Melee *>(m_teams[i][j]->getWeapon())->getDurability()) + ")" : "") <<
					std::endl;
			}
			else {
				std::cout <<
					(m_teams[i][j]->getStatus()[poison] != 0 ? "P" : " ") <<
					(m_teams[i][j]->getStatus()[focus] != 0 ? "F" : " ") <<
					(m_teams[i][j]->getStatus()[invisibility] != 0 ? "I" : " ") <<
					(m_teams[i][j]->getStatus()[aim] != 0 ? "A" : " ") <<
					"\t" << m_teams[i][j]->getName() << (m_teams[i][j]->getName().length() >= 8 ? "" : "\t") << (m_teams[i][j]->getName().length() >= 16 ? "" : "\t") <<
					"\t" << (m_teams[i][j]->getSpeed() == m_teams[i][j]->getStats()[p_speed] ? 0 : m_teams[i][j]->getSpeed()) << "\t" << (m_teams[i][j]->getLife() < 0 ? 0 : m_teams[i][j]->getLife()) << "/" << m_teams[i][j]->getStats()[p_life] << "\t" << (isWizard ? std::to_string(m_teams[i][j]->getMana()) + "/" + std::to_string(m_teams[i][j]->getStats()[p_mana]) : "") <<
					std::endl;
			}
		}
		std::cout << std::endl;
	}

	// Display who's turn it is
	std::cout << std::endl << "Tour de : joueur " << character.player + 1 << ", " << m_teams[character.player][character.character_id]->getName() << ". Actions possibles :" << std::endl;

	// Display possible actions
	switch (mode) {
	case 0b00:
		std::cout <<
			"\t- Attaquer" << std::endl <<
			"\t- Competences" << std::endl <<
			"\t- Sac" << std::endl <<
			"\t- Passer" << std::endl <<
			std::endl;
		break;
	case 0b01:
		for (auto& skill : skills) {
			if (skill.second.m_cooldown > 0) {
				std::cout << "\t- " << skill.first << " (" << skill.second.m_cooldown << ")" << std::endl;
			}
			else {
				std::cout << "\t- " << skill.first << std::endl;
			}
		}
		std::cout << "\t- Fermer" << std::endl << std::endl;
		break;
	case 0b10:
		for (auto& item : *items) {
			if (item.second.m_usable) {
				std::cout << "\t- " << item.first << " (" << item.second.m_quantity << ")" << std::endl;
			}
		}
		std::cout << "\t- Fermer" << std::endl << std::endl;
		break;
	default:
		break;
	}

	return success;
}

Player *Game::getItemTarget(playing_character_position character, const std::string& itemname)
{
	// User input
	std::string user_input;

	// Potion de vie
	if (itemname == "Potion de vie") {
		std::cout << "Sur qui utiliser l'objet ?" << std::endl;
		std::getline(std::cin, user_input);
		// Amongst allies
		for (int i = 0; i < 3; ++i) {
			if (m_teams[character.player][i]->getName() == user_input) {
				return m_teams[character.player][i];
			}
		}
	}
	// Potion de mana
	else if (itemname == "Potion de mana") {
		std::cout << "Sur qui utiliser l'objet ?" << std::endl;
		std::getline(std::cin, user_input);
		// Amongst allies
		for (int i = 0; i < 3; ++i) {
			if (m_teams[character.player][i]->getName() == user_input) {
				return m_teams[character.player][i];
			}
		}
	}
	// Parchemin de resurrection
	else if (itemname == "Parchemin de resurrection") {
		std::cout << "Sur qui utiliser l'objet ?" << std::endl;
		std::getline(std::cin, user_input);
		// Amongst allies
		for (int i = 0; i < 3; ++i) {
			if (m_teams[character.player][i]->getName() == user_input) {
				return m_teams[character.player][i];
			}
		}
	}
	// Antidote
	else if (itemname == "Antidote") {
		std::cout << "Sur qui utiliser l'objet ?" << std::endl;
		std::getline(std::cin, user_input);
		// Amongst allies
		for (int i = 0; i < 3; ++i) {
			if (m_teams[character.player][i]->getName() == user_input) {
				return m_teams[character.player][i];
			}
		}
	}
	// Flechette empoisonnee
	else if (itemname == "Flechette empoisonnee") {
		std::cout << "Sur qui utiliser l'objet ?" << std::endl;
		std::getline(std::cin, user_input);
		// Amongst ennemies
		for (int i = 0; i < 3; ++i) {
			if (m_teams[(character.player == 0 ? 1 : 0)][i]->getName() == user_input) {
				return m_teams[(character.player == 0 ? 1 : 0)][i];
			}
		}
	}
	return nullptr;
}

Player *Game::getSkillTarget(playing_character_position character, const std::string& skillname)
{
	// User input
	std::string user_input;

	// Scream
	if (skillname == CD_STRING[Scream]) {
		return m_teams[character.player][character.character_id];
	}
	// Fix
	else if (skillname == CD_STRING[Fix]) {
		std::cout << "Sur qui utiliser la competence ?" << std::endl;
		std::getline(std::cin, user_input);
		// Amongst allies
		for (int i = 0; i < 3; ++i) {
			if (m_teams[character.player][i]->getName() == user_input) {
				return m_teams[character.player][i];
			}
		}
	}
	// Aim
	else if (skillname == CD_STRING[Aim]) {
		return m_teams[character.player][character.character_id];
	}
	// Antidote
	else if (skillname == CD_STRING[Antidote]) {
		std::cout << "Sur qui utiliser la competence ?" << std::endl;
		std::getline(std::cin, user_input);
		// Amongst allies
		for (int i = 0; i < 3; ++i) {
			if (m_teams[character.player][i]->getName() == user_input) {
				return m_teams[character.player][i];
			}
		}
	}
	// Heal
	else if (skillname == CD_STRING[Heal]) {
		std::cout << "Sur qui utiliser la competence ?" << std::endl;
		std::getline(std::cin, user_input);
		// Amongst allies
		for (int i = 0; i < 3; ++i) {
			if (m_teams[character.player][i]->getName() == user_input) {
				return m_teams[character.player][i];
			}
		}
	}
	// RegenMana
	else if (skillname == CD_STRING[RegenMana]) {
		return m_teams[character.player][character.character_id];
	}
	// Enchant
	else if (skillname == CD_STRING[Enchant]) {
		std::cout << "Sur qui utiliser la competence ?" << std::endl;
		std::getline(std::cin, user_input);
		// Amongst allies
		for (int i = 0; i < 3; ++i) {
			if (m_teams[character.player][i]->getName() == user_input) {
				return m_teams[character.player][i];
			}
		}
	}
	// Invisility
	else if (skillname == CD_STRING[Invisibility]) {
		return m_teams[character.player][character.character_id];
	}

	return nullptr;
}

std::string Game::mp_getInput(std::vector<struct playing_character_position> character_queue) const
{
	sf::Packet packet;
	std::string packet_buffer, user_input;

	// Server in send mode
	if (m_server && character_queue.back().player == 0) {
		// Get input
		std::getline(std::cin, user_input);
		// Wait for ready
		while (packet_buffer != "RDY") {
			packet << "SOT";
			m_net->getClient().send(packet);
			packet.clear();
			m_net->getClient().receive(packet);
			packet >> packet_buffer;
			packet.clear();
		}
		// Send
		packet << user_input;
		m_net->getClient().send(packet);
		packet.clear();
		packet << "EOT";
		m_net->getClient().send(packet);
		packet.clear();
	}
	// Client in send mode
	else if (!m_server && character_queue.back().player == 1) {
		// Get input
		std::getline(std::cin, user_input);
		// Wait for ready
		while (packet_buffer != "RDY") {
			packet << "SOT";
			m_net->getSocket().send(packet);
			packet.clear();
			m_net->getSocket().receive(packet);
			packet >> packet_buffer;
			packet.clear();
		}
		// Send
		packet << user_input;
		m_net->getSocket().send(packet);
		packet.clear();
		packet << "EOT";
		m_net->getSocket().send(packet);
		packet.clear();
	}
	// Server in receive mode
	else if (m_server && character_queue.back().player == 1) {
		std::cout << "En attente d'un autre joueur" << std::endl;
		// Wait for start of transmission
		while (packet_buffer != "SOT") {
			packet << "RDY";
			m_net->getClient().send(packet);
			packet.clear();
			m_net->getClient().receive(packet);
			packet >> packet_buffer;
			packet.clear();
		}
		// Receive the input
		while (packet_buffer != "EOT") {
			packet_buffer = "";
			while (packet_buffer.empty()) {
				m_net->getClient().receive(packet);
				packet >> packet_buffer;
				packet.clear();
			}
			if (packet_buffer == "EOT") {
				break;
			}
			// Handle the input
			user_input = packet_buffer;
		}
	}
	// Client in receive mode
	else {
		std::cout << "En attente d'un autre joueur" << std::endl;
		// Wait for start of transmission
		while (packet_buffer != "SOT") {
			packet << "RDY";
			m_net->getSocket().send(packet);
			packet.clear();
			m_net->getSocket().receive(packet);
			packet >> packet_buffer;
			packet.clear();
		}
		// Receive the input
		while (packet_buffer != "EOT") {
			packet_buffer = "";
			while (packet_buffer.empty()) {
				m_net->getSocket().receive(packet);
				packet >> packet_buffer;
				packet.clear();
			}
			if (packet_buffer == "EOT") {
				break;
			}
			// Handle the input
			user_input = packet_buffer;
		}
	}
	return user_input;
}
