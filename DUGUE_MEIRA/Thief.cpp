#include "Thief.h"
#include <utility>

#include "Skill.h"
#include "Dagger.h"
#include "Bow.h"


Thief::Thief(std::string name, int *stats) :
	Player(std::move(name), stats)
{
	Thief::initSkills();
}

void Thief::attack(Player* target) {
	// Declare variables for random number generation
	std::default_random_engine random_generator;
	const std::uniform_int_distribution<int> distribution(85, 100);
	const std::uniform_int_distribution<int> dodge_distrib(target->getStats()[p_dodge], 100);
	const bool dodge = dodge_distrib(random_generator) < target->getStats()[p_dodge];
	// Compute coeff
	const int coeff = distribution(random_generator);

	// If the player doesn't have a weapon
	if (!m_weapon) {
		if (!dodge) {
			target->setLife(static_cast<float>(target->getLife()) - 15.f * (static_cast<float>(m_stats[p_attack]) / static_cast<float>(target->getStats()[p_defense]))*(static_cast<float>(coeff) / 100.f));
		}
	}
	// If the weapon is a bow
	if (dynamic_cast<Bow *>(m_weapon)) {
		// If he has arrows
		if (m_bag.use("Fleche") == success) {
			if (!dodge) {
				target->setLife(static_cast<float>(target->getLife()) - 7.f * ((static_cast<float>(m_stats[p_dexterity]) + static_cast<float>(m_weapon->use())) / static_cast<float>(target->getStats()[p_defense])) * (static_cast<float>(coeff) / 100.f));
			}
			else {
				m_weapon->use();
			}
		}
		// If he has no arrows
		else {
			if (!dodge) {
				target->setLife(static_cast<float>(target->getLife()) - 15.f * (static_cast<float>(m_stats[p_attack]) / static_cast<float>(target->getStats()[p_defense]))*(static_cast<float>(coeff) / 100.f));
			}
		}
	}
	// If the weapon is a dagger
	if (dynamic_cast<Dagger *>(m_weapon)) {
		// Compute poison
		const std::uniform_int_distribution<int> distribution_poison(0, 100);
		if (distribution_poison(random_generator) <= 15) {
			target->getStatus()[poison] = 1;
		}
		if (!dodge) {
			target->setLife(static_cast<float>(target->getLife()) - 7.f * (static_cast<float>(m_stats[p_dexterity] + static_cast<float>(m_weapon->use())) / static_cast<float>(target->getStats()[p_defense])) * (static_cast<float>(coeff) / 100.f));
		}
		else {
			m_weapon->use();
		}
		if (getStatus()[enchanted] != 0) {
			m_weapon->getStatsW()[w_damage] *= static_cast<int>(3.f / 4.f);
			getStatus()[enchanted] = 0;
		}
	}
}

void Thief::initSkills() {
	m_skills.emplace(CD_STRING[Invisibility], s_Skill(&thief_Invisibility, 0));
}