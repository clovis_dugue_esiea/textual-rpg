#include "Bow.h"
#include <utility>
#include "Player.h"

Bow::Bow(std::string name, int *stats, int *wstats) :
	Ranged(std::move(name), stats, wstats)
{
}

int Bow::use()
{
	// Return damage
	return m_stats[w_damage];
}

std::set<std::string> Bow::getCompatibility()
{
	std::set<std::string> v;
	v.insert("Archer");
	v.insert("Thief");
	return v;
}