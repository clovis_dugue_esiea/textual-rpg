#include "Skill.h"
#include "Bow.h"
#include "Archer.h"
#include "Melee.h"

// WARRIOR
void warrior_Scream(Player* p, Player *t) {
	if (!t) {
		t = p;
	}
	if (p->getSkills().at(CD_STRING[Scream]).m_cooldown > 0) {
		std::cout << "Vous ne pouvez plus lancer ce sort." << std::endl;
		return;
	}
	// Declare variables for random number generation
	std::default_random_engine random_generator;
	const std::uniform_int_distribution<int> distribution(1, 2);

	// Compute value
	const int value = distribution(random_generator);

	t->setStatus(focus, value);
	p->getSkills().at(CD_STRING[Scream]).m_cooldown = 4;
}

void warrior_Fix(Player* p, Player *t) {
	t->getWeapon()->setDurability();
}

// ARCHER
void archer_Aim(Player* p, Player *t) {
	if (!t) {
		t = p;
	}
	if (dynamic_cast<Bow *>(p->getWeapon())) {
		if (p->getStatus()[aim] != 0) {
			std::cout << "Le bonus de ce sort est deja actif." << std::endl;
			return;
		}
		p->getStats()[p_dexterity] = static_cast<int>(p->getStats()[p_dexterity] * 1.33);
		p->getStatus()[aim] = 1;
	}
}

void archer_Antidote(Player* p, Player *t) {
	if (dynamic_cast<Archer *>(p)->m_antidote_spell_use_count <= 0) {
		std::cout << "Vous ne pouvez plus lancer ce sort." << std::endl;
		return;
	}
	t->getStatus()[poison] = 0;
	dynamic_cast<Archer *>(p)->m_antidote_spell_use_count--;
}

// WIZARD
void wizard_Heal(Player* p, Player *t) {
	if (p->getSkills().at(CD_STRING[Heal]).m_cooldown > 0 || p->getMana() < 5) {
		std::cout << "Vous ne pouvez plus lancer ce sort." << std::endl;
		return;
	}

	// Declare variables for random number generation
	std::default_random_engine random_generator;
	const std::uniform_int_distribution<int> distribution(10, 20);

	// Compute value
	const int value = distribution(random_generator);

	// Restore HP, decrease mana
	if (t->getLife() >= t->getStats()[p_life] - value) {
		t->setLife(t->getLife() + t->getStats()[p_life] - t->getLife());
	}
	else {
		t->setLife(t->getLife() + value);
	}
	p->setMana(p->getMana() - 5);
	p->getSkills().at(CD_STRING[Heal]).m_cooldown = 4;
}

void wizard_RegenMana(Player* p, Player *t) {
	if (!t) {
		t = p;
	}
	// Declare variables for random number generation
	std::default_random_engine random_generator;
	const std::uniform_int_distribution<int> distribution(2, 7);

	// Compute value
	const int value = distribution(random_generator);

	if (p->getMana() >= p->getStats()[p_mana] - value) {
		p->setMana(p->getMana() + p->getStats()[p_mana] - p->getMana());
	}
	else {
		p->setMana(p->getMana() + value);
	}
}

void wizard_Enchant(Player* p, Player *t) {
	if (p->getSkills().at(CD_STRING[Enchant]).m_cooldown > 0 || p->getMana() < 7) {
		std::cout << "Vous ne pouvez plus lancer ce sort." << std::endl;
		return;
	}
	if (!dynamic_cast<Melee *>(t->getWeapon())) {
		std::cout << "Ce soir doit etre lance sur une arme de melee." << std::endl;
		return;
	}

	t->getStatus()[enchanted] = 1;
	t->getWeapon()->getStatsW()[w_damage] *= static_cast<int>(4.f / 3.f);
	p->setMana(p->getMana() - 7);
	p->getSkills().at(CD_STRING[Enchant]).m_cooldown = 3;
}

// THIEF
void thief_Invisibility(Player* p, Player *t) {
	if (!t) {
		t = p;
	}
	if (p->getSkills().at(CD_STRING[Invisibility]).m_cooldown > 0) {
		std::cout << "Vous ne pouvez plus lancer ce sort." << std::endl;
		return;
	}

	// Declare variables for random number generation
	std::default_random_engine random_generator;
	const std::uniform_int_distribution<int> distribution(1, 2);

	// Compute value
	const int value = distribution(random_generator);

	p->setStatus(invisibility, value);
	p->getSkills().at(CD_STRING[Invisibility]).m_cooldown = 4;
}