#include "Bag.h"
#include <utility>
#include "Player.h"

Bag::s_items::s_items(int quantity, std::string type, bool usable):
	m_quantity(quantity),
	m_type(std::move(type)),
	m_usable(usable) {}

int Bag::use(const std::string& itemname, Player *player)
{
	// Initialize return value
	int ret;

	// Check if the item is in the bag
	if (m_items.find(itemname) == m_items.end()) {
		return err_itemNotInBag;
	}

	// Switch action for each item
	--m_items.at(itemname).m_quantity;
	// Arrows
	if (itemname == "Fleche") {
		ret = success;
	}
	// Potion de vie
	else if (itemname == "Potion de vie") {
		if (!player) {
			return err_itemNeedsATarget;
		}
		if (player->getLife() >= player->getStats()[p_life] - 30) {
			player->setLife(player->getLife() + player->getStats()[p_life] - player->getLife());
		}
		else {
			player->setLife(player->getLife() + 30);
		}
		ret = success;
	}
	// Potion de mana
	else if (itemname == "Potion de mana") {
		if (!player) {
			return err_itemNeedsATarget;
		}
		if (player->getMana() >= player->getStats()[p_mana] - 20) {
			player->setMana(player->getMana() + player->getStats()[p_mana] - player->getMana());
		}
		else {
			player->setMana(player->getMana() + 20);
		}
		ret = success;
	}
	// Parchemin de resurrection
	else if (itemname == "Parchemin de resurrection") {
		if (!player) {
			return err_itemNeedsATarget;
		}
		if (player->getLife() <= 0) {
			player->setLife(player->getStats()[p_life]);
			ret = success;
		}
		else {
			++m_items.at(itemname).m_quantity;
			ret = err_itemCantBeUsed;
		}
	}
	// Antidote
	else if (itemname == "Antidote") {
		if (!player) {
			return err_itemNeedsATarget;
		}
		if (player->getStatus()[poison] != 0) {
			player->setStatus(poison, 0);
			ret = success;
		}
		else {
			++m_items.at(itemname).m_quantity;
			ret = err_itemCantBeUsed;
		}
	}
	// Flechette empoisonnee
	else if (itemname == "Flechette empoisonnee") {
		if (!player) {
			return err_itemNeedsATarget;
		}
		player->setStatus(poison, 1);
		ret = success;
	}
	// Unknown item in bag
	else {
		ret = err_itemNotImplemented;
	}

	// Delete every item with 0 quantity from the bag
	if (ret == success && m_items.at(itemname).m_quantity == 0) {
		m_items.erase(m_items.find(itemname));
	}

	// Return
	return ret;
}

void Bag::addItem(std::string name, std::string type, bool usable) {
	m_items.insert(std::make_pair(name, s_items(1, std::move(type), usable)));
}

std::map<std::string, struct Bag::s_items> * Bag::getItems() { return &m_items; }