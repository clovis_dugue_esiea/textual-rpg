#pragma once

#include <utility>
#include "utils.h"
#include "Weapon.h"
#include "Bag.h"

/**
 * \brief	Donnees importantes liees a un personnage
 */
class Player {
protected:
	/**
	 * \brief	Definition du type de pointeur sur fonction correspondant a une aptitude
	 */
	typedef void (*ScriptFunction)(Player*, Player*);
	/**
	 * \brief	Informations necessaires a l'affichage et l'instanciation des aptitudes
	 */
	struct s_Skill
	{
		/**
		 * \brief	Pointeur sur la fonction correspondant a une aptitude
		 */
		ScriptFunction m_skill;
		/**
		 * \brief	Temps de rechargement de l'aptitude
		 */
		int m_cooldown;
		/**
		 * \brief	Constructeur de la structure aptitude
		 * \param skill		Membre m_skill
		 * \param cooldown	Membre m_cooldown
		 */
		s_Skill(ScriptFunction skill, int cooldown);
	};
	/**
	 * \brief	Pointeur sur l'arme equipee
	 */
	Weapon* m_weapon;
	/**
	 * \brief	Sac
	 */
	Bag m_bag;
	/**
	 * \brief	Nom
	 */
	std::string m_name;
	/**
	 * \brief	Statistiques (vie max, agilite, attaque...)
	 */
	int m_stats[NB_STATS];
	/**
	 * \brief	Status (invisibilite, provocation, poison...)
	 */
	int m_status[NB_STATUS];
	/**
	 * \brief	Map des aptitudes accessibles au personnage
	 */
	std::map<std::string, s_Skill> m_skills;
	/**
	 * \brief	Vie actuelle
	 */
	int m_current_life;
	/**
	 * \brief	Vitesse actuelle
	 */
	int m_speed;
public:
	/**
	 * \brief	Constructeur
	 * \param name	Nom
	 * \param stats	Statistiques
	 */
	Player(std::string name, const int *stats);
	/**
	 * \brief	Destructeur
	 */
	virtual ~Player();
	/**
	 * \brief	Gestion de l'attaque en fonction de sa classe et de son arme eventuelle
	 * \param target	Cible de l'attaque
	 */
	virtual void attack(Player* target) = 0;
	/**
	 * \brief	Desequipe et libere l'arme
	 */
	void unequipWeapon();
	/**
	 * \brief	Remplissage du tableau d'aptitudes disponibles (m_skills) en fonction de sa classe
	 */
	virtual	void initSkills() = 0;
	/**
	 * \brief	Decrementation de la vitesse actuelle (m_speed), remise au maximum si arrivee a zero
	 */
	void decrementSpeed();
	// Getters
	/**
	 * \brief	Getter pour le membre m_name
	 * \return	m_nom
	 */
	virtual std::string getName();
	/**
	 * \brief	Getter pour le membre m_stats
	 * \return	m_stats
	 */
	virtual int* getStats();
	/**
	 * \brief	Getter pour le membre m_status
	 * \return	m_status
	 */
	virtual int* getStatus();
	/**
	 * \brief	Getter pour le membre m_bag
	 * \return	m_bag
	 */
	virtual Bag* getBag();
	/**
	 * \brief	Getter pour le membre m_weapon
	 * \return	m_weapon
	 */
	Weapon* getWeapon() const;
	/**
	 * \brief	Getter pour le membre m_speed
	 * \return	m_speed
	 */
	int getSpeed() const;
	/**
	 * \brief	Getter pour le membre m_current_life
	 * \return	m_current_life
	 */
	int getLife() const;
	/**
	 * \brief	Getter pour le membre m_mana, uniquement chez le magicien
	 * \return	0
	 */
	virtual int getMana();
	/**
	 * \brief	Getter pour le membre m_skills
	 * \return	m_skills
	 */
	virtual std::map<std::string, s_Skill>& getSkills();
	// Setters
	/**
	 * \brief	Setter pour le membre m_life
	 * \param value		Valeur a remplir (sera caste en entier)
	 */
	void setLife(float value);
	/**
	 * \brief	Setter pour le membre m_life
	 * \param value		Valeur a remplir
	 */
	void setLife(int value);
	/**
	 * \brief	Setter pour le membre m_weapon
	 * \param weapon	Pointeur sur l'arme a equiper
	 */
	void setWeapon(Weapon *weapon);
	/**
	 * \brief	Setter pour un status particulier
	 * \param index		Status a changer
	 * \param stat		Valeur a inserer
	 */
	virtual void setStatus(int index, int stat);
	/**
	 * \brief	Setter pour le membre m_mana (magicien uniquement)
	 * \param value		Valeur a inserer
	 */
	virtual void setMana(int value);
	/**
	 * \brief	Passe la vitesse a son maximum
	 */
	void setSpeedFull();

};