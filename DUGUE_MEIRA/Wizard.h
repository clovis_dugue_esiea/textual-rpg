#pragma once

#include "Player.h"

/**
 * \brief	Magicien
 */
class Wizard : public Player {
protected:
	/**
	 * \brief	Mana actuel
	 */
	int m_mana{};

public:
	/**
	 * \brief	Constructeur
	 * \param name	Nom
	 * \param stats	Statistiques
	 */
	Wizard(std::string name, int *stats);
	/**
	 * \brief	Destructeur
	 */
	~Wizard() = default;

	/**
	 * \brief	Gestion de l'attaque du magicien en fonction de son arme eventuelle
	 * \param target	Cible de l'attaque
	 */
	void attack(Player* target) override;
	/**
	 * \brief	Remplissage du tableau d'aptitudes disponibles (m_skills) du magicien
	 */
	void initSkills() override;

	/**
	 * \brief	Getter pour le membre m_mana, uniquement chez le magicien
	 * \return	m_mana
	 */
	int getMana() override;

	/**
	 * \brief	Setter pour le membre m_mana (magicien uniquement)
	 * \param value		Valeur a inserer
	 */
	void setMana(int value) override;
};