#include "Player.h"
#include <utility>
#include "Wizard.h"
#include "Warrior.h"
#include "Thief.h"
#include "Archer.h"

Player::s_Skill::s_Skill(ScriptFunction skill, int cooldown):
	m_skill(skill),
	m_cooldown(cooldown) {}

Player::Player(std::string name, const int *stats) :
	m_weapon(nullptr),
	m_bag(Bag()),
	m_name(std::move(name))
{
	for (int i(0); i < NB_STATS; ++i) {
		m_stats[i] = stats[i];
	}
	m_speed = m_stats[p_speed];
	m_current_life = m_stats[p_life];
}

Player::~Player() {
	delete m_weapon;
	m_weapon = nullptr;
	//m_skills.clear();
}

void Player::unequipWeapon()
{
	if (m_weapon) {
		delete m_weapon;
		m_weapon = nullptr;
	}
}

std::string Player::getName() { return m_name; }

int * Player::getStats() { return m_stats; }

int* Player::getStatus() { return m_status; }

Bag * Player::getBag() { return &m_bag; }

void Player::setStatus(int index, int stat) {
	m_status[index] = stat;
}

void Player::setLife(float value) {
	m_current_life = static_cast<int>(value);
}

void Player::setLife(int value) {
	m_current_life = value;
}

Weapon* Player::getWeapon() const
{
	return m_weapon;
}

void Player::setWeapon(Weapon *weapon) { m_weapon = weapon; }

void Player::decrementSpeed() { --m_speed; }

int Player::getSpeed() const { return m_speed; }

int Player::getLife() const { return m_current_life; }

void Player::setMana(int value) { }

void Player::setSpeedFull() { m_speed = m_stats[p_speed]; }

int Player::getMana() { return 0; }

std::map<std::string, Player::s_Skill>& Player::getSkills() { return m_skills; }
