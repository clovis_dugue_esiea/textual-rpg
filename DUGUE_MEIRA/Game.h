#pragma once
#include <utility>
#include "Player.h"
#include "Archer.h"
#include "Warrior.h"
#include "Thief.h"
#include "Wizard.h"
#include "Staff.h"
#include "Bow.h"
#include "Dagger.h"
#include "Sword.h"
#include "Skill.h"
#include "Network.h"

/**
 * \brief	Definition du type de pointeur sur fonction correspondant a une aptitude
 */
typedef void (*ScriptFunction)(Player*, Player*);
/**
 * \brief	Definition de la structure contenant les informations de position dans le tableau m_teams d'un personnage
 */
typedef struct playing_character_position playing_character_position;

/**
 * \brief	Donnees importantes liees a une partie
 */
class Game
{
protected:
	/**
	 * \brief	Informations necessaires a l'affichage et l'instanciation des aptitudes
	 */
	struct s_Skill
	{
		/**
		 * \brief	Pointeur sur la fonction correspondant a une aptitude
		 */
		ScriptFunction m_skill;
		/**
		 * \brief	Temps de rechargement de l'aptitude
		 */
		int m_cooldown;
		/**
		 * \brief	Constructeur de la structure aptitude
		 * \param skill		Membre m_skill
		 * \param cooldown	Membre m_cooldown
		 */
		s_Skill(ScriptFunction skill, int cooldown);
	};


	/**
	 * \brief 	Informations necessaires a l'affichage et l'instanciation des objets
	 */
	struct s_items
	{
		/**
		 * \brief 	Prix de l'objet
		 */
		int m_price;
		/**
		 * \brief 	Type de l'objet : Objet (Item) ou Arme (Weapon)
		 */
		std::string m_type;
		/**
		 * \brief	L'objet est-il utilisable en tant que tel (Potion) ou non (Fleche)
		 */
		bool m_usable;
		/**
		 * \brief 	Constructeur de la structure objet
		 * \param price 		Membre m_price
		 * \param type 			Membre m_type
		 * \param usable 		Membre m_usable
		 */
		s_items(int price, std::string type, bool usable);
	};

	/**
	 * \brief	Informations necessaires a l'utilisation de la librairie reseau de SFML
	 */
	Network *m_net;
	/**
	 * \brief	Cette partie est-elle multijoueur ?
	 */
	bool m_multiplayer;
	/**
	 * \brief	Cette instance du jeu est-elle serveur ?
	 */
	bool m_server;

	/**
	 * \brief	Informations de tous les personnages disponibles
	 */
	std::map<std::string, struct s_Player *>m_characters;
	/**
	 * \brief	Informations de toutes les armes disponibles
	 */
	std::map<std::string, struct s_Weapon *>m_weapons;
	/**
	 * \brief	Informations de tous les objets disponibles a l'achat
	 */
	std::map<std::string, struct s_items>m_shop;
	/**
	 * \brief	Pointeurs vers les fonctions de chaque aptitude
	 */
	std::map<std::string, s_Skill> m_skills;
	/**
	 * \brief	Historique des actions effectuees, dans shop() et equipWeapons()
	 */
	std::map<std::string, void *> m_transactions;

	/**
	 * \brief	Pointeurs vers les joueurs de chaque equipe 
	 */
	Player *m_teams[2][3]{};
	/**
	 * \brief	Argent disponible pour chaque joueur
	 */
	int m_money[2]{};
public:
	/**
	 * \brief	Constructeur
	 */
	Game();
	/**
	 * \brief	Destructeur
	 */
	~Game();

	// Initialization

	/**
	 * \brief	Choix du mode de jeu. Stoque le resultat dans m_multiplayer, et potentiellement m_server
	 * \return	Booleen indiquant la bonne execution de la fonction
	 */
	bool chooseMode();
	/**
	 * \brief	Initialise la partie en chargeant toutes les informations, et en gerrant les choix de l'utilisateur
	 * \param step		Entier correspondant a la fonction a appeler
	 * \param chosen	Entier indiquant le nombre de personnages deja choisis
	 * \return	Code d'erreur indiquant le deroulement de la fonction
	 */
	int start(int step = 0, int chosen = 0);
	/**
	 * \brief	Chargement des objets disponibles a l'achat depuis un fichier
	 * \param filename	Fichier depuis lequel charger les objets disponibles a l'achat
	 */
	void initializeShop(const std::string& filename = "resources/shop.jdc");
	/**
	 * \brief	Remplit la map de pointeurs sur les fonctions associees aux differentes aptitudes (m_skills)
	 */
	void initSkills();

	/**
	 * \brief	Choix des personnage de l'utilisateur
	 * \param chosen	Entier indiquant le nombre de personnages deja choisis
	 * \return	Code d'erreur indiquant le deroulement de la fonction
	 */
	int chooseTeam(int chosen = 0);
	/**
	 * \brief	Choix des objets de chacun des personnages
	 * \return	Code d'erreur indiquant le deroulement de la fonction
	 */
	int shop();
	/**
	 * \brief	Equipement des eventuelles armes achetees dans la boutique
	 * \return	Code d'erreur indiquant le deroulement de la fonction
	 */
	int equipWeapons();

	/**
	 * \brief	Chargement des informations des personnages depuis un fichier
	 * \param filename	Fichier depuis lequel charger les informations des personnages
	 */
	void loadPlayer(std::string const& filename = "resources/characters.jdc");
	/**
	 * \brief	Chargement des informations des armes depuis un fichier
	 * \param filename	Fichier depuis lequel charger les informations des objets
	 */
	void loadWeapon(std::string const& filename = "resources/weapons.jdc");

	/**
	 * \brief	Affichage des personnages
	 * \param mode	Mode d'affichage : Tous / un seul personnage, toutes / moins d'informations
	 * \param character		Dans le cas d'un seul personnage, son nom
	 */
	void printCharacters(int mode = 0b00, const std::string& character = "");
	/**
	 * \brief	Affichage des armes
	 * \param mode	Mode d'affichage : Tous / une seule arme, toutes / moins d'informations
	 * \param weapon		Dans le cas d'une seule arme, son nom
	 */
	void printWeapons(int mode = 0b00, const std::string& weapon = "");

	// Game
	/**
	 * \brief	Lancement de la partie
	 * \return	Code d'erreur indiquant le deroulement de la fonction, ou le joueur gagnant
	 */
	int play();
	/**
	 * \brief	Affichage du plateau de jeu
	 * \param character		Personnage dont c'est le tour
	 * \param mode			Mode d'affichage : Toutes les actions, objets disponibles, aptitudes disponibles
	 * \param err			Potentielle erreur a afficher en haut de la console
	 * \return	Code d'erreur indiquant le deroulement de la fonction
	 */
	int displayBoard(playing_character_position character, unsigned short mode = 0b00, const std::string& err = "");

	/**
	 * \brief	Demande si necessaire la cible d'un objet, et la verifie
	 * \param character		Personnage utilisant l'objet
	 * \param itemname		Objet utilise
	 * \return	Joueur cible
	 */
	Player *getItemTarget(playing_character_position character, const std::string& itemname);
	/**
	 * \brief	Demande si necessaire la cible d'une aptitude, et la verifie
	 * \param character		Personnage utilisant l'aptitude
	 * \param skillname		Aptitude utilisee
	 * \return	Joueur cible
	 */
	Player *getSkillTarget(playing_character_position character, const std::string& skillname);

	/**
	 * \brief	Recuperation de l'entree utilisateur en mode multijoueur
	 * \param character_queue	Information sur la liste de personnages devant jouer
	 * \return	Entree utilisateur recuperee
	 */
	std::string mp_getInput(std::vector<struct playing_character_position> character_queue) const;
};