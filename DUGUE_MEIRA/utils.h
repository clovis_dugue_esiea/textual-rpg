#pragma once

#include <string>
#include <random>
#include <set>
#include <map>
#include <iostream>
#include <fstream>
#include <functional>
#include <utility>

#include "ConsoleColor.h"

#define STARTING_MONEY 1000

/**
 * \brief	Definition de la classe Player
 */
class Player;

/**
 * \brief	Enumeration des codes d'erreur
 */
enum err
{
	fuck = -1,
	success,
	err_itemNotInBag,
	err_itemNotImplemented,
	err_wrongClassFormat,
	err_itemCantBeUsed,
	err_itemNeedsATarget,
	err_nullptrSkillTarget
};

// Define a string and an enum member for each type of stat (preprocessor)
#define FOREACH_STAT(STAT) \
	STAT(p_life)				\
	STAT(p_attack)				\
	STAT(p_defense)				\
	STAT(p_dodge)				\
	STAT(p_speed)				\
	STAT(p_strength)			\
	STAT(p_dexterity)			\
	STAT(p_luck)				\
	STAT(p_intelligence)		\
	STAT(p_mana)				\
	STAT(NB_STATS)
#define GENERATE_ENUM(ENUM) ENUM,
#define GENERATE_STRING(STRING) #STRING,
/**
 * \brief	Enumeration d'entiers correspondant aux diverses statistiques de personnage
 */
enum STAT_ENUM {
	FOREACH_STAT(GENERATE_ENUM)
};
/**
 * \brief	Tableau de string contenant le nom des diverses statistiques de personnage
 */
static const std::string STAT_STRING[] = {
	FOREACH_STAT(GENERATE_STRING)
};

// Define a string and an enum member for each type of stat (preprocessor)
#define FOREACH_STATW(STATW) \
	STATW(w_damage)			\
	STATW(w_critical)		\
	STATW(w_cost)			\
	STATW(w_durability)		\
	STATW(w_poisonchance)	\
	STATW(NB_STATSW)
#define GENERATE_ENUM(ENUM) ENUM,
#define GENERATE_STRING(STRING) #STRING,
/**
 * \brief	Enumeration d'entiers correspondant aux diverses statistiques d'arme
 */
enum STATW_ENUM {
	FOREACH_STATW(GENERATE_ENUM)
};
/**
 * \brief	Tableau de string contenant le nom des diverses statistiques d'arme
 */
static const std::string STATW_STRING[] = {
	FOREACH_STATW(GENERATE_STRING)
};

// Define a string and an enum member for each type of stat (preprocessor)
#define FOREACH_CD(CD) \
	CD(Scream)			\
	CD(Fix)				\
	CD(Aim)				\
	CD(Antidote)		\
	CD(Heal)			\
	CD(RegenMana)		\
	CD(Enchant)			\
	CD(Invisibility)	\
	CD(NB_CD)
#define GENERATE_ENUM(ENUM) ENUM,
#define GENERATE_STRING(STRING) #STRING,
/**
 * \brief	Enumeration d'entiers correspondant aux diverses aptitudes
 */
enum CD_ENUM {
	FOREACH_CD(GENERATE_ENUM)
};
/**
 * \brief	Tableau de string contenant le nom des diverses aptitudes
 */
static const std::string CD_STRING[] = {
	FOREACH_CD(GENERATE_STRING)
};

// Define a string and an enum member for each type of stat (preprocessor)
#define FOREACH_STATUS(STATUS) \
	STATUS(poison)				\
	STATUS(focus)				\
	STATUS(invisibility)		\
	STATUS(aim)					\
	STATUS(enchanted)			\
	STATUS(NB_STATUS)
#define GENERATE_ENUM(ENUM) ENUM,
#define GENERATE_STRING(STRING) #STRING,
/**
 * \brief	Enumeration d'entiers correspondant aux divers status
 */
enum STATUS_ENUM {
	FOREACH_STATUS(GENERATE_ENUM)
};
/**
 * \brief	Tableau de string contenant le nom des divers status
 */
static const std::string STATUS_STRING[] = {
	FOREACH_STATUS(GENERATE_STRING)
};

/**
 * \brief	Informations sur les armes, utilisees pour l'affichage
 */
struct s_Weapon
{
	/**
	 * \brief	Nom
	 */
	std::string m_name;
	/**
	 * \brief	Type
	 */
	std::string m_type;
	/**
	 * \brief	Statistiques bonus
	 */
	int m_stats[NB_STATS]{};
	/**
	 * \brief	Statistiques
	 */
	int m_wstats[NB_STATSW]{};
	/**
	 * \brief	Constructeur
	 * \param name		Membre m_name
	 * \param type		Membre m_type
	 * \param stats		Membre m_stats
	 * \param wstats	Membre m_wstats
	 */
	s_Weapon(std::string name, std::string type, const int *stats, const int *wstats) :
		m_name(std::move(name)),
		m_type(std::move(type))
	{
		for (int i = 0; i < NB_STATS; ++i) {
			m_stats[i] = stats[i];
		}
		for (int i = 0; i < NB_STATSW; ++i) {
			m_wstats[i] = wstats[i];
		}
	}
};

/**
 * \brief	Informations sur les personnages, utilisees pour l'affichage
 */
struct s_Player
{
	/**
	 * \brief	Nom
	 */
	std::string m_name;
	/**
	 * \brief	Classe
	 */
	std::string m_type;
	/**
	 * \brief	Statistiques
	 */
	int m_stats[NB_STATS]{};
	/**
	 * \brief	Constructeur
	 * \param name		Membre m_name
	 * \param type		Membre m_type
	 * \param stats		Membre m_stats
	 */
	s_Player(std::string name, std::string type, const int *stats) :
		m_name(std::move(name)),
		m_type(std::move(type))
	{
		for (int i = 0; i < NB_STATS; ++i) {
			m_stats[i] = stats[i];
		}
	}
};

/**
 * \brief	Separe une chaine de caracteres en un vecteur de chaines
 * e.g. : "stringSplit("Split me this", " ") outputs vec[0]=="Split", vec[1]=="me", vec[2]=="this"
 * \param str	Chaine a separer
 * \param sep	Separateur a prendre en compte
 * \return	Vector de std::string
 */
std::vector<std::string> stringSplit(const std::string& str, const std::string& sep);