#include "Melee.h"
#include <utility>

Melee::Melee(std::string name, int *stats, int *wstats) :
	Weapon(std::move(name), stats, wstats),
	m_durability(wstats[w_durability])
{
}

void Melee::setDurability() {
	// Declare variables for random number generation
	std::default_random_engine random_generator;
	const std::uniform_int_distribution<int> distribution(3, 15);

	// Compute wearing
	const int durabilityr = distribution(random_generator);

	if (m_durability <= 0) {
		m_durability = 1;
	}
	else {
		if (m_durability + durabilityr <= m_wstats[w_durability])
			m_durability += durabilityr;
		else {
			m_durability = m_wstats[w_durability];
		}
	}
}

int Melee::getDurability() { return m_durability; }
