#include "Staff.h"
#include <utility>
#include "Player.h"

Staff::Staff(std::string name, int *stats, int *wstats) :
	Ranged(std::move(name), stats, wstats)
{
}

int Staff::use()
{
	// Return damage
	return m_stats[w_damage];
}

std::set<std::string> Staff::getCompatibility()
{
	std::set<std::string> v;
	v.insert("Wizard");
	return v;
}