#pragma once

#include <SFML/Network.hpp>
#include <iostream>
#include <sstream>

/**
 * \brief	Classe contenant les utilitaires reseau
 */
class Network
{
private:
	/**
	 * \brief	Adresse IP LAN
	 */
	sf::IpAddress m_serverIPLAN;
	/**
	 * \brief	Adresse IP WAN
	 */
	sf::IpAddress m_serverIPWAN;
	/**
	 * \brief	Port du serveur (utiliseer 53530)
	 */
	unsigned short m_serverPORT;
	
	// Server side
	/**
	 * \brief	Socket TCP client
	 */
	sf::TcpSocket m_client;
	/**
	 * \brief	Adresse IP client
	 */
	sf::IpAddress m_clientIP;
	
	// Client side
	/**
	 * \brief	Socket TCP serveur
	 */
	sf::TcpSocket m_socket;
public:
	/**
	 * \brief	Constructeur
	 */
	Network();
	/**
	 * \brief	Destructeur
	 */
	~Network() = default;

	/**
	 * \brief	Initialisation d'une instance serveur
	 * \return	Vrai si reussie
	 */
	bool initServer();
	/**
	 * \brief	Initialisation d'une instance client
	 * \param err	Erreur a afficher
	 * \return	Vrai si reussie
	 */
	bool initClient(const std::string& err = "");

	/**
	 * \brief	Getter pour le membre m_client
	 * \return	m_client
	 */
	sf::TcpSocket& getClient();
	/**
	 * \brief	Getter pour le membre m_socket
	 * \return	m_socket
	 */
	sf::TcpSocket& getSocket();
};

