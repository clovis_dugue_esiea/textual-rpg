#include "Wizard.h"
#include <utility>
#include "Skill.h"
#include "Staff.h"
#include "Sword.h"

Wizard::Wizard(std::string name, int *stats) :
	Player(std::move(name), stats),
	m_mana(stats[p_intelligence] * 10)
{
	m_stats[p_mana] = stats[p_intelligence] * 10;
	initSkills();
}

void Wizard::attack(Player* target) {
	// Declare variables for random number generation
	std::default_random_engine random_generator;
	const std::uniform_int_distribution<int> distribution(85, 100);
	const std::uniform_int_distribution<int> dodge_distrib(target->getStats()[p_dodge], 100);
	bool dodge = dodge_distrib(random_generator) < target->getStats()[p_dodge];

	// Compute coeff
	const int coeff = distribution(random_generator);

	// If the player doesn't have a weapon
	if (m_weapon == nullptr || m_mana < m_weapon->getStatsW()[w_cost]) {
		if (!dodge) {
			target->setLife(static_cast<float>(target->getLife()) - 15.f * (static_cast<float>(m_stats[p_attack]) / static_cast<float>(target->getStats()[p_defense])) * (static_cast<float>(coeff) / 100.f));
		}
	}
	// If the weapon is a staff
	if (dynamic_cast<Staff *>(m_weapon)) {
		m_mana -= m_weapon->getStatsW()[w_cost];
		if (!dodge) {
			target->setLife(static_cast<float>(target->getLife()) - 5.f * ((static_cast<float>(m_stats[p_intelligence]) + static_cast<float>(m_weapon->use())) / static_cast<float>(target->getStats()[p_defense])) * (static_cast<float>(coeff) / 100.f));
		}
		else {
			m_weapon->use();
		}
	}
	// If the weapon is a sword
	if (dynamic_cast<Sword *>(m_weapon)) {
		if (!dodge) {
			target->setLife(static_cast<float>(target->getLife()) - 5.f * ((static_cast<float>(m_stats[p_intelligence]) + static_cast<float>(m_weapon->use())) / static_cast<float>(target->getStats()[p_defense])) * (static_cast<float>(coeff) / 100.f));
		}
		else {
			m_weapon->use();
		}
		if (getStatus()[enchanted] != 0) {
			m_weapon->getStatsW()[w_damage] *= static_cast<int>(3.f / 4.f);
			getStatus()[enchanted] = 0;
		}
	}
}

void Wizard::initSkills() {
	m_skills.emplace(CD_STRING[Heal], s_Skill(&wizard_Heal, 0));
	m_skills.emplace(CD_STRING[RegenMana], s_Skill(&wizard_RegenMana, 0));
	m_skills.emplace(CD_STRING[Enchant], s_Skill(&wizard_Enchant, 0));
}

int Wizard::getMana() { return m_mana; }

void Wizard::setMana(int value) { m_mana = value; }