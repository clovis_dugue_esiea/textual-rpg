#include "utils.h"

std::vector<std::string> stringSplit(const std::string& str, const std::string& sep) {
	// Valeur de retour
	std::vector<std::string> ret;
	// Bornes
	size_t inf = 0, sup = 0;
	while (sup <= str.length()) {
		sup = str.find_first_of(sep, inf);
		ret.push_back(str.substr(inf, sup - inf));
		inf = sup + 1;
		// Effacement des caractères blancs
		if (ret.back().empty()) {
			ret.pop_back();
		}
	}
	return ret;
}