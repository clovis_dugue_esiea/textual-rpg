#pragma once

#include "Player.h"

/**
 * \brief	Archer
 */
class Archer : public Player {
public:
	/**
	 * \brief	L'archer utilise-t-il actuellement un arc
	 */
	bool m_bowequipped;
	/**
	 * \brief	Nombre d'utilisations de l'aptitude Antidote
	 */
	int m_antidote_spell_use_count;

	/**
	 * \brief	Constructeur
	 * \param name	Nom
	 * \param stats	Statistiques
	 */
	Archer(std::string name, int *stats);
	/**
	 * \brief	Destructeur
	 */
	~Archer() = default;

	/**
	 * \brief	Gestion de l'attaque de l'archer en fonction de son arme eventuelle
	 * \param target	Cible de l'attaque
	 */
	void attack(Player* target) override;
	/**
	 * \brief	Remplissage du tableau d'aptitudes disponibles (m_skills) du guerrier
	 */
	void initSkills() override;
};