#include "Dagger.h"
#include <utility>
#include "Player.h"

Dagger::Dagger(std::string name, int *stats, int *wstats) :
	Melee(std::move(name), stats, wstats)
{
}

int Dagger::use()
{
	// Decrease durability
	if (m_durability <= 1) {
		m_durability = 0;
	}
	else {
		--m_durability;
	}

	// Return damage
	return m_stats[w_damage];
}

std::set<std::string> Dagger::getCompatibility()
{
	std::set<std::string> v;
	v.insert("Archer");
	v.insert("Thief");
	v.insert("Warrior");
	return v;
}