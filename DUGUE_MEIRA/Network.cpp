#include "Network.h"
#include "ConsoleColor.h"

Network::Network() : m_serverPORT(53530) {}

bool Network::initServer() {
	system(clearcommand);
	std::string user_input;
	
	sf::TcpListener TCPListener;

	m_serverIPLAN = sf::IpAddress::getLocalAddress();
	m_serverIPWAN = sf::IpAddress::getPublicAddress();

	while (TCPListener.listen(m_serverPORT) != sf::Socket::Done) {
		std::cout << "Unable to open port \"" << m_serverPORT << "\"." << std::endl;
		while (user_input.find_first_not_of("0123456789")) {
			std::cout << "Enter the port manually :\n> ";
			std::cin >> user_input;
			std::cin.clear();
		}
		m_serverPORT = (unsigned short)stoi(user_input);
	}

	std::cout <<
		"Address to connect :\n" <<
		"\tLAN(" << m_serverIPLAN << ")" <<
		"\tWAN(" << m_serverIPWAN << ")" <<
		"\tPORT(" << m_serverPORT << ")\n\n" <<
		"Waiting for new client." <<
		std::endl;

	sf::TcpSocket TCPClient;
	// Accept new client
	if (TCPListener.accept(m_client) == sf::Socket::Done) {
		m_clientIP = m_client.getRemoteAddress();
		m_client.setBlocking(true);
		sf::Packet packet;
		packet << 1;
		m_client.send(packet);
		}
	return(true);
}

bool Network::initClient(const std::string& err) {
	system(clearcommand);
	std::string user_input;

	std::cout << err << std::endl;
	std::cout << "Enter the address IP of the server :\n> ";
	std::cin >> user_input;
	std::cin.clear();
	m_serverIPLAN = user_input;
	std::cout << "\nEnter the port of the server :\n> ";
	std::cin >> user_input;
	std::cin.clear();
	m_serverPORT = (unsigned short)stoi(user_input);

	// Lunch the socket
	sf::Socket::Status TCPStatus = m_socket.connect(m_serverIPLAN, m_serverPORT);

	if (TCPStatus != sf::Socket::Done) {
		std::stringstream ss;
		ss << "\nUnable to connect at \"" << m_serverIPLAN << "\" with port \"" << m_serverPORT << "\"";
		initClient(ss.str());
	}

	sf::Packet packet;
	m_socket.receive(packet);
	int data = 0;
	packet >> data;

	if (data == 1) {
		return(true);
	}
	return(false);
}

sf::TcpSocket& Network::getClient() {
	return m_client;
}

sf::TcpSocket& Network::getSocket() {
	return m_socket;
}