#pragma once

#include "Player.h"

/**
 * \brief	Voleur
 */
class Thief : public Player {
public:
	/**
	 * \brief	Constructeur
	 * \param name	Nom
	 * \param stats	Statistiques
	 */
	Thief(std::string name, int *stats);
	/**
	 * \brief	Destructeur
	 */
	~Thief() = default;

	/**
	 * \brief	Gestion de l'attaque du voleur en fonction de son arme eventuelle
	 * \param target	Cible de l'attaque
	 */
	void attack(Player* target) override;
	/**
	 * \brief	Remplissage du tableau d'aptitudes disponibles (m_skills) du guerrier
	 */
	void initSkills() override;
};