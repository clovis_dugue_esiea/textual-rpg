#pragma once
#include "Weapon.h"

/**
 * \brief	Methodes associees aux armes de melee
 */
class Melee : public Weapon
{
protected:
	/**
	 * \brief	Durabilite actuelle de l'arme
	 */
	int m_durability;
public:
	/**
	 * \brief	Constructeur
	 * \param name		Nom de l'arme
	 * \param stats		Statistiques bonus ajoutees a celles du joueur
	 * \param wstats	Statistiques de l'arme
	 */
	Melee(std::string name, int *stats, int *wstats);
	/**
	 * \brief	Destructeur
	 */
	virtual ~Melee() = default;

	/**
	 * \brief	Utilisation de l'arme (overridee)
	 * \return	Degats de l'arme
	 */
	int use() override = 0;

	/**
	 * \brief	Donne la liste des classes pouvant porter cette arme (overridee)
	 * \return	Set de nom de classes pouvant s'equiper l'arme
	 */
	std::set<std::string>  getCompatibility() override = 0;
	/**
	 * \brief	Getter pour le membre m_durability
	 * \return	m_durability
	 */
	virtual int getDurability();
	
	/**
	 * \brief	Reparation de l'arme par une valeur aleatoire entre 3 et 15
	 */
	void setDurability() override;
};

