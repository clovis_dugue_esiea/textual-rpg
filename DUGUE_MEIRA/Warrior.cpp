#include "Warrior.h"
#include <utility>
#include "Skill.h"
#include "Sword.h"
#include "Dagger.h"

Warrior::Warrior(std::string name, int *stats) :
	Player(std::move(name), stats)
{
	Warrior::initSkills();
}

void Warrior::attack(Player* target) {
	// Declare variables for random number generation
	std::default_random_engine random_generator;
	const std::uniform_int_distribution<int> distribution(85, 100);
	const std::uniform_int_distribution<int> dodge_distrib(target->getStats()[p_dodge], 100);
	const bool dodge = dodge_distrib(random_generator) < target->getStats()[p_dodge];

	// Compute coeff
	const int coeff = distribution(random_generator);

	// If the player doesn't have a weapon
	if (m_weapon == nullptr) {
		if (!dodge) {
		target->setLife(static_cast<float>(target->getLife()) - 15.f * (static_cast<float>(m_stats[p_attack]) / static_cast<float>(target->getStats()[p_defense])) * (static_cast<float>(coeff) / 100.f));
		}
	}
	// If the weapon is a dagger
	if (dynamic_cast<Dagger *>(m_weapon)) {
		if (!dodge) {
			target->setLife(static_cast<float>(target->getLife()) - 5.f * ((static_cast<float>(m_stats[p_attack]) + static_cast<float>(m_weapon->use())) / static_cast<float>(target->getStats()[p_defense])) * (static_cast<float>(coeff) / 100.f));
		}
		else {
			m_weapon->use();
		}
		if (getStatus()[enchanted] != 0) {
			m_weapon->getStatsW()[w_damage] *= static_cast<int>(3.f / 4.f);
			getStatus()[enchanted] = 0;
		}
	}
	// If the weapon is a sword
	if (dynamic_cast<Sword *>(m_weapon)) {
		if (!dodge) {
		target->setLife(static_cast<float>(target->getLife()) - 5.f * ((static_cast<float>(m_stats[p_attack]) + static_cast<float>(m_weapon->use())) / static_cast<float>(target->getStats()[p_defense])) * (static_cast<float>(coeff) / 100.f));
		}
		else {
			m_weapon->use();
		}
		if (getStatus()[enchanted] != 0) {
			m_weapon->getStatsW()[w_damage] *= static_cast<int>(3.f / 4.f);
			getStatus()[enchanted] = 0;
		}
	}
}

void Warrior::initSkills() {
	m_skills.emplace(CD_STRING[Scream], s_Skill(&warrior_Scream, 0));
	m_skills.emplace(CD_STRING[Fix], s_Skill(&warrior_Fix, 0));
}