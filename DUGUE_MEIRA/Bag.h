#pragma once

#include <utility>
#include "utils.h"

/**
 * \brief	Definition de la classe Player
 */
class Player;

/**
 * \brief	Sac du personnage
 */
class Bag
{
protected:
	/**
	 * \brief 	Informations necessaires a l'affichage et l'instanciation des objets
	 */
	struct s_items
	{
		/**
		 * \brief 	Quantite dans le sac
		 */
		int m_quantity;
		/**
		 * \brief 	Type de l'objet : Objet (Item) ou Arme (Weapon)
		 */
		std::string m_type;
		/**
		 * \brief	L'objet est-il utilisable en tant que tel (Potion) ou non (Fleche)
		 */
		bool m_usable;
		/**
		 * \brief 	Constructeur de la structure objet
		 * \param quantity 		Membre m_quantity
		 * \param type 			Membre m_type
		 * \param usable 		Membre m_usable
		 */
		s_items(int quantity, std::string type, bool usable);
	};
	/**
	 * \brief	Map des objets presents dans le sac
	 */
	std::map<std::string, struct s_items> m_items;

public:
	/**
	 * \brief	Constructeur
	 */
	Bag() = default;
	/**
	 * \brief	Destructeur
	 */
	~Bag() = default;

	/**
	 * \brief	Utilisation d'un objet. Decremente le nombre d'objets, le retire du sac si 0
	 * \param itemname	Nom de l'objet a utiliser
	 * \param player	Cible de l'objet (null si soi-meme)
	 * \return	Code d'erreur indiquant le deroulement de la fonction
	 */
	int use(const std::string& itemname, Player *player = nullptr);
	/**
	 * \brief	Ajout d'un objet dans le sac a partir de son nom et son type
	 * \param name		Nom de l'objet a ajouter
	 * \param type		Type de l'objet : Objet (Item) ou Arme (Weapon)
	 * \param usable	L'objet est-il utilisable en tant que tel (Potion) ou non (Fleche)
	 */
	void addItem(std::string name, std::string type, bool usable);
	/**
	 * \brief	Getter pour le membre m_items
	 * \return	m_items
	 */
	std::map<std::string, struct s_items> *getItems();
};

