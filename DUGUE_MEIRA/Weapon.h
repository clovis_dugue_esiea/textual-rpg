#pragma once

#include "utils.h"

class Weapon
{
protected:
	/**
	 * \brief	Nom de l'arme
	 */
	std::string m_name;
	/**
	 * \brief	Statistiques bonus ajoutees a celles du joueur
	 */
	int m_stats[NB_STATS]{};
	/**
	 * \brief	Statistiques de l'arme
	 */
	int m_wstats[NB_STATSW]{};
	/**
	 * \brief	Liste des classes pouvant porter cette arme
	 */
	std::vector<std::string> m_compatibility;
public:
	/**
	 * \brief	Constructeur par defaut inutilisable
	 */
	Weapon() = delete;
	/**
	 * \brief	Constructeur
	 * \param name		Nom de l'arme
	 * \param stats		Statistiques bonus ajoutees a celles du joueur
	 * \param wstats	Statistiques de l'arme
	 */
	Weapon(std::string name, const int *stats, const int *wstats);
	/**
	 * \brief	Destructeur
	 */
	virtual ~Weapon() = default;

	/**
	 * \brief	Utilisation de l'arme (overridee)
	 * \return	Degats de l'arme
	 */
	virtual int use() = 0;

	/**
	 * \brief	Getter pour le membre m_name
	 * \return	m_name
	 */
	virtual std::string getName();
	/**
	 * \brief	Getter pour le membre m_stats
	 * \return	m_stats
	 */
	int* getStats();
	/**
	 * \brief	Getter pour le membre m_statsw
	 * \return	m_statsw
	 */
	int* getStatsW();
	/**
	 * \brief	Donne la liste des classes pouvant porter cette arme (overridee)
	 * \return	Set de nom de classes pouvant s'equiper l'arme
	 */
	virtual std::set<std::string>  getCompatibility() = 0;

	/**
	 * \brief	Reparation de l'arme par une valeur aleatoire entre 3 et 15 (overridee dans la classe fille Melee)
	 */
	virtual void setDurability() {};
};
