#include "Game.h"
#include "utils.h"

Game::s_items::s_items(int price, std::string type, bool usable):
	m_price(price),
	m_type(std::move(type)),
	m_usable(usable) {} // NOLINT(cppcoreguidelines-pro-type-member-init)

Game::s_Skill::s_Skill(ScriptFunction skill, int cooldown):
	m_skill(skill),
	m_cooldown(cooldown) {}

Game::Game() :
	m_net(nullptr),
	m_multiplayer(false),
	m_server(false)
{
	m_net = new Network();
	for (int i = 0; i < 2; ++i) {
		for (int j = 0; j < 3; ++j) {
			m_teams[i][j] = nullptr;
		}
	}
	m_money[0] = STARTING_MONEY;
	m_money[1] = STARTING_MONEY;
}

Game::~Game()
{
	struct s_transaction_shop
	{
		int character_id = -1;
		int price = -1;
		std::string item_name;
	};
	struct s_transaction_equipWeapons
	{
		int item_owner_id = -1;
		int weapon_owner_id = -1;
		std::string item_name;
		std::string weapon_name;
	};

	for (auto& m_team : m_teams) {
		for (auto& j : m_team) {
			delete j;
		}
	}
	for (auto& m_character : m_characters) {
		delete m_character.second;
	}
	for (auto& m_weapon : m_weapons) {
		delete m_weapon.second;
	}
	for (auto& m_transactions : m_transactions) {
		if (m_transactions.first == "shop") {
			delete static_cast<s_transaction_shop *>(m_transactions.second);
		}
		else if (m_transactions.first == "equipWeapons") {
			delete static_cast<s_transaction_equipWeapons *>(m_transactions.second);
		}
	}
	delete m_net;
}

bool Game::chooseMode() {
	std::string user_input;
	std::cout <<
		"1. Mode CAMPAGNE\n" <<
		"2. Mode RESEAU\n" <<
		"3. Quit" <<
		std::endl;

	std::cout << "> ";
	std::getline(std::cin, user_input);

	while (user_input != "1" && user_input != "2" && user_input != "3") {
		std::cout << "\n> ";
		std::cin >> user_input;
		std::cin.clear();
	}

	switch (std::stoi(user_input)) {
	case 1:
		m_multiplayer = false;
		return(true);
	case 2:
		m_multiplayer = true;
		system(clearcommand);
		std::cout <<
			"1. Heberger une partie\n" <<
			"2. Rejoindre une partie\n" <<
			"3. Return" <<
			std::endl;

		std::cout << "> ";
		std::getline(std::cin, user_input);
		std::cin.clear();

		while (user_input != "1" && user_input != "2" && user_input != "3") {
			std::cout << "> ";
			std::getline(std::cin, user_input);
			std::cin.clear();
		}

		switch (std::stoi(user_input)) {
		case 1:
			m_server = true;
			return(m_net->initServer());
		case 2:
			return(m_net->initClient());
		case 3:
			chooseMode();
		default:
			break;
		}
	case 3:
		return(false);
	default: 
		break;
	}
	return(false);
}

int Game::start(int step, int chosen)
{
	int errcode = success;

	// Load players and weapons list
	loadPlayer();
	loadWeapon();

	// Initialize shop
	initializeShop();

	// Choose your characters
	if (step == 0) {
		errcode = chooseTeam(chosen);
		++step;
	}
	switch (errcode) {
	case fuck:
		return fuck;
	case success:
		break;
	default:
		break;
	}

	// Shop
	if (step == 1) {
		errcode = shop();
		++step;
	}
	switch (errcode) {
	case fuck:
		start(--step - 1, 2 * 3);
	case success:
		break;
	default:
		break;
	}

	// Equip bought weapons
	if (step == 2) {
		errcode = equipWeapons();
		++step;
	}
	switch (errcode) {
	case fuck:
		start(--step - 1, 2 * 3);
	case success:
		break;
	default:
		break;
	}

	// Launch the game with those teams
	if (step == 3) {
		errcode = play();
		++step;
	}
	switch (errcode) {
	case fuck:
		start(--step - 1, 2 * 3);
	case success:
		break;
	default:
		break;
	}

	return errcode;
}

void Game::initializeShop(const std::string& filename)
{
	std::ifstream file;
	std::string line;

	file.open(filename);
	if (!file.is_open()) {
		std::cout << "Error loadPlayer() -> Unable to opening file " << filename << std::endl;
		return;
	}

	while (!file.eof()) {
		getline(file, line);
		std::vector<std::string> v_line = stringSplit(line, ":");

		m_shop.insert(std::make_pair(v_line[0], Game::s_items(std::stoi(v_line[1]), "Item", v_line[2] != "false")));
	}
	file.close();
}

void Game::initSkills() {
	m_skills.emplace(CD_STRING[Scream], s_Skill(&warrior_Scream, 0));
	m_skills.emplace(CD_STRING[Fix], s_Skill(&warrior_Fix, 0));
	m_skills.emplace(CD_STRING[Aim], s_Skill(&archer_Aim, 0));
	m_skills.emplace(CD_STRING[Antidote], s_Skill(&archer_Antidote, 0));
	m_skills.emplace(CD_STRING[Heal], s_Skill(&wizard_Heal, 0));
	m_skills.emplace(CD_STRING[RegenMana], s_Skill(&wizard_RegenMana, 0));
	m_skills.emplace(CD_STRING[Enchant], s_Skill(&wizard_Enchant, 0));
	m_skills.emplace(CD_STRING[Invisibility], s_Skill(&thief_Invisibility, 0));
}

int Game::chooseTeam(int chosen)
{
	// User input variable
	std::string user_input;
	// Counting variable
	int characters_chosen = chosen;
	// If all players have chosen their teams
	bool ready = false, jump = false;
	// If there was an error in the user input
	std::string err;
	// Character being focussed
	std::string current_character;
	// Temporary variables for copy
	std::string class_tmp;
	std::string name_tmp;
	int istats_tmp[NB_STATS];
	// Multiplayer
	std::string packet_buffer;

	// Change the init value according to SP or MP mode
	if (m_multiplayer && !m_server) {
		characters_chosen = 3;
	}
	// Choose the characters
	while (!ready) {
		// Clear screen
		system(clearcommand);
		std::cout <<
			// Display possible error
			err << std::endl <<
			// Choosing message
			"Choisir un personnage ! Joueur " << characters_chosen / 3 + 1 << ", personnage " << characters_chosen % 3 + 1 <<
			std::endl;
		// Display the current teams
		for (int i = 0; i < 2; ++i) {
			std::cout << std::endl << "Joueur " << i + 1 << std::endl;
			for (int j = 0; j < 3; ++j) {
				if (m_teams[i][j]) {
					std::cout << "\t- " << m_teams[i][j]->getName() << std::endl;
				}
			}
		}
		std::cout << std::endl;
		err = "";
		// Display characters (reduced)
		printCharacters(0b10);
		std::cout << std::endl <<
			// Stats message
			"Entrer le nom d'un personnage pour voir ses caracteristiques" <<
			std::endl;
		// User input
		std::getline(std::cin, user_input);
		current_character = user_input;
		if (current_character.empty()) {
			continue;
		}
		// If the user is really rude
		if (user_input == "fuck") {
			if (characters_chosen > 0) {
				--characters_chosen;
			}
			else {
				return fuck;
			}
			delete m_teams[characters_chosen / 3][characters_chosen % 3];
			m_teams[characters_chosen / 3][characters_chosen % 3] = nullptr;
		}
		// If the character doesn't exist
		else if (m_characters.find(current_character) == m_characters.end()) {
			// Error message
			err = "Le personnage choisi n'existe pas !";
		}
		// If the character exists
		else {
			// Check if the player already has the character
			for (int i = 0; i < 3; ++i) {
				if (m_teams[characters_chosen / 3][i]) {
					if (current_character == m_teams[characters_chosen / 3][i]->getName()) {
						err = "Vous avez deja ce personnage dans votre equipe";
						jump = true;
						break;
					}
				}
			}
			if (jump) {
				jump = false;
				continue;
			}
			// Clear screen	
			system(clearcommand);
			// Print it in more details
			printCharacters(0b01, user_input);
			// Ask to add it to the team
			std::cout <<
				"Je te veux dans mon equipe ! [y/n]" <<
				std::endl;
			// User input
			while (user_input != "y" && user_input != "n" && user_input != "fuck") {
				std::getline(std::cin, user_input);
			}
			// If the player accepted
			if (user_input == "y") {
				// Add the character to the team if he doesn't
				//m_teams[characters_chosen / 3][characters_chosen % 3] = m_characters.at(current_character);
				class_tmp = m_characters.at(current_character)->m_type;
				name_tmp = m_characters.at(current_character)->m_name;
				for (int i = 0; i < NB_STATS; ++i) {
					istats_tmp[i] = m_characters.at(current_character)->m_stats[i];
				}
				if (class_tmp == "Warrior") {
					m_teams[characters_chosen / 3][characters_chosen % 3] = new Warrior(name_tmp, istats_tmp);
				}
				else if (class_tmp == "Wizard") {
					m_teams[characters_chosen / 3][characters_chosen % 3] = new Wizard(name_tmp, istats_tmp);
				}
				else if (class_tmp == "Thief") {
					m_teams[characters_chosen / 3][characters_chosen % 3] = new Thief(name_tmp, istats_tmp);
				}
				else if (class_tmp == "Archer") {
					m_teams[characters_chosen / 3][characters_chosen % 3] = new Archer(name_tmp, istats_tmp);
				}
				else {
					return err_wrongClassFormat;
				}
				++characters_chosen;
			}
			// If the player refused
			else if (user_input == "n" || user_input == "fuck") {
				// Go back
				continue;
			}
		}
		// If all the players have chosen
		if (m_multiplayer && m_server) {
			if (characters_chosen == 3) {
				// Let's fight
				ready = true;
			}
		}
		else {
			if (characters_chosen == 2 * 3) {
				// Let's fight
				ready = true;
			}
		}
	}

	// If the game is in multiplayer, exchange team data
	if (m_multiplayer) {
		sf::Packet packet;
		system(clearcommand);
		std::cout << std::endl << "En attente d'un autre joueur" << std::endl;
		current_character = "";
		if (m_server) {
			// Wait for ready
			while (packet_buffer != "RDY") {
				packet << "SOT";
				m_net->getClient().send(packet);
				packet.clear();
				m_net->getClient().receive(packet);
				packet >> packet_buffer;
				packet.clear();
			}
			// Send
			for (int i = 0; i < 3; ++i) {
				packet << m_teams[0][i]->getName();
				m_net->getClient().send(packet);
				packet.clear();
			}
			packet << "EOT";
			m_net->getClient().send(packet);
			packet.clear();
		}
		else {
			int received = -1;
			// Wait for start of transmission
			while (packet_buffer != "SOT") {
				packet << "RDY";
				m_net->getSocket().send(packet);
				packet.clear();
				m_net->getSocket().receive(packet);
				packet >> packet_buffer;
				packet.clear();
			}
			// Receive the input
			while (packet_buffer != "EOT") {
				packet_buffer = "";
				while (packet_buffer.empty()) {
					m_net->getSocket().receive(packet);
					packet >> packet_buffer;
					packet.clear();
				}
				if (packet_buffer == "EOT") {
					break;
				}
				// Handle the input
				++received;
				current_character = packet_buffer;
				// Add the character to the team if he doesn't
				class_tmp = m_characters.at(current_character)->m_type;
				name_tmp = m_characters.at(current_character)->m_name;
				for (int i = 0; i < NB_STATS; ++i) {
					istats_tmp[i] = m_characters.at(current_character)->m_stats[i];
				}
				if (class_tmp == "Warrior") {
					m_teams[0][received] = new Warrior(name_tmp, istats_tmp);
				}
				else if (class_tmp == "Wizard") {
					m_teams[0][received] = new Wizard(name_tmp, istats_tmp);
				}
				else if (class_tmp == "Thief") {
					m_teams[0][received] = new Thief(name_tmp, istats_tmp);
				}
				else if (class_tmp == "Archer") {
					m_teams[0][received] = new Archer(name_tmp, istats_tmp);
				}
				else {
					return err_wrongClassFormat;
				}
			}
		}
		current_character = "";
		if (!m_server) {
			// Wait for ready
			while (packet_buffer != "RDY") {
				packet << "SOT";
				m_net->getSocket().send(packet);
				packet.clear();
				m_net->getSocket().receive(packet);
				packet >> packet_buffer;
				packet.clear();
			}
			// Send
			for (int i = 0; i < 3; ++i) {
				packet << m_teams[1][i]->getName();
				m_net->getSocket().send(packet);
				packet.clear();
			}
			packet << "EOT";
			m_net->getSocket().send(packet);
			packet.clear();
		}
		else {
			int received = -1;
			// Wait for start of transmission
			while (packet_buffer != "SOT") {
				packet << "RDY";
				m_net->getClient().send(packet);
				packet.clear();
				m_net->getClient().receive(packet);
				packet >> packet_buffer;
				packet.clear();
			}
			// Receive the input
			while (packet_buffer != "EOT") {
				packet_buffer = "";
				while (packet_buffer.empty()) {
					m_net->getClient().receive(packet);
					packet >> packet_buffer;
					packet.clear();
				}
				if (packet_buffer == "EOT") {
					break;
				}
				// Handle the input
				++received;
				current_character = packet_buffer;
				// Add the character to the team if he doesn't
				class_tmp = m_characters.at(current_character)->m_type;
				name_tmp = m_characters.at(current_character)->m_name;
				for (int i = 0; i < NB_STATS; ++i) {
					istats_tmp[i] = m_characters.at(current_character)->m_stats[i];
				}
				if (class_tmp == "Warrior") {
					m_teams[1][received] = new Warrior(name_tmp, istats_tmp);
				}
				else if (class_tmp == "Wizard") {
					m_teams[1][received] = new Wizard(name_tmp, istats_tmp);
				}
				else if (class_tmp == "Thief") {
					m_teams[1][received] = new Thief(name_tmp, istats_tmp);
				}
				else if (class_tmp == "Archer") {
					m_teams[1][received] = new Archer(name_tmp, istats_tmp);
				}
				else {
					return err_wrongClassFormat;
				}
			}
		}
	}
	return success;
}

int Game::shop()
{
	// User input variable
	std::string user_input;
	// Who's turn it is
	int player = 0;
	// If all players have chosen their items
	bool ready = false, jump = false;
	// If there was an error in the user input
	std::string err;
	// Remember the last transaction details in order to be able to cancel it
	struct s_transaction
	{
		int character_id = -1;
		int price = -1;
		std::string item_name;
	};
	std::vector<struct s_transaction> *transaction_history;
	if (m_transactions.find("shop") == m_transactions.end()) {
		transaction_history = new std::vector<struct s_transaction>;
		m_transactions.insert(std::make_pair("shop", transaction_history));
	}
	else {
		transaction_history = reinterpret_cast<std::vector<struct s_transaction>*>(m_transactions.at("shop"));
		player = 1;
		transaction_history->pop_back();
	}
	struct s_transaction last_transaction;

	// Change the init value according to SP or MP mode
	if (m_multiplayer && !m_server) {
		player = 1;
	}
	// Choose the weapons
	while (!ready) {
		// Clear screen
		system(clearcommand);
		std::cout <<
			// Display possible error
			err << std::endl <<
			// Choosing message
			"C'est les soldes ! Au tour du Joueur " << player + 1 <<
			std::endl;
		err = "";
		// Display the shop contents
		for (auto &it : m_shop) {
			if (it.second.m_type == "Weapon") {
				std::cout <<
					m_weapons.at(it.first)->m_type << "\t" << it.second.m_price << "\t : " << it.first <<
					std::endl;
			}
		}
		for (auto &it : m_shop) {
			if (it.second.m_type != "Weapon") {
				std::cout <<
					it.second.m_type << "\t" << it.second.m_price << "\t : " << it.first <<
					std::endl;
			}
		}
		std::cout << std::endl;
		// Display the current bag contents
		for (int i = 0; i < 2; ++i) {
			std::cout << std::endl << "Joueur " << i + 1 << ", " << m_money[i] << " restant" << std::endl;
			for (int j = 0; j < 3; ++j) {
				if (m_teams[i][j]) {
					std::cout << "\t- " << m_teams[i][j]->getName() << std::endl;
					for (auto& it : *m_teams[i][j]->getBag()->getItems()) {
						if (it.second.m_type == "Weapon") {
							std::cout << "\t\t- (" << it.second.m_quantity << ")\t" << m_weapons.at(it.first)->m_type << "\t" << it.first << std::endl;
						}
					}
					for (auto& it : *m_teams[i][j]->getBag()->getItems()) {
						if (it.second.m_type != "Weapon") {
							std::cout << "\t\t- (" << it.second.m_quantity << ")\t" << it.second.m_type << "\t" << it.first << std::endl;
						}
					}
				}
			}
		}
		std::cout << std::endl << "Quel objet acheter ?" << std::endl;
		err = "";
		// User input
		std::getline(std::cin, user_input);
		std::string current_item = user_input;
		if (current_item.empty()) {
			continue;
		}
		// If the user is really rude
		if (user_input == "fuck" && !transaction_history->empty()) {
			// If the last operation was changing whose turn it is to buy
			if (transaction_history->back().item_name == "done") {
				--player;
			}
			// If it was a regular purchase
			else {
				--m_teams[player][transaction_history->back().character_id]->getBag()->getItems()->at(transaction_history->back().item_name).m_quantity;
				if (m_teams[player][transaction_history->back().character_id]->getBag()->getItems()->at(transaction_history->back().item_name).m_quantity == 0) {
					m_teams[player][transaction_history->back().character_id]->getBag()->getItems()->erase(transaction_history->back().item_name);
				}
				m_money[player] += transaction_history->back().price;
			}
			transaction_history->pop_back();
		}
		else if (user_input == "fuck" && transaction_history->empty()) {
			delete transaction_history;
			m_transactions.erase("shop");
			return fuck;
		}
		// If the user wants to stop shopping
		else if (user_input == "done") {
			last_transaction.character_id = -1;
			last_transaction.item_name = "done";
			last_transaction.price = -1;
			transaction_history->push_back(last_transaction);
			if (player == 0 && !m_server) {
				++player;
			}
			else {
				ready = true;
			}
		}
		// If the item doesn't exist
		else if (m_shop.find(current_item) == m_shop.end()) {
			err = "L'objet choisi n'existe pas !";
		}
		// If the item exists
		else {
			if (m_weapons.find(current_item) != m_weapons.end()) {
				system(clearcommand);
				printWeapons(0b01, current_item);
				// Ask to buy the weapon
				std::cout <<
					"Acheter l'arme ? [y/n]" <<
					std::endl;
				// User input
				while (user_input != "y" && user_input != "n" && user_input != "fuck") {
					std::getline(std::cin, user_input);
				}
				// If he refuses
				if (user_input == "n" || user_input == "fuck") {
					// Go back
					continue;
				}
				// Clear screen
				system(clearcommand);
				std::cout <<
					// Display possible error
					err << std::endl <<
					// Choosing message
					"C'est les soldes ! Au tour du Joueur " << player + 1 <<
					std::endl;
				err = "";
				// Display the shop contents
				for (auto &it : m_shop) {
					if (it.second.m_type == "Weapon") {
						std::cout <<
							m_weapons.at(it.first)->m_type << "\t" << it.second.m_price << "\t : " << it.first <<
							std::endl;
					}
				}
				for (auto &it : m_shop) {
					if (it.second.m_type != "Weapon") {
						std::cout <<
							it.second.m_type << "\t" << it.second.m_price << "\t : " << it.first <<
							std::endl;
					}
				}
				std::cout << std::endl;
				// Display the current bag contents
				for (int i = 0; i < 2; ++i) {
					std::cout << std::endl << "Joueur " << i + 1 << ", " << m_money[i] << " restant" << std::endl;
					for (int j = 0; j < 3; ++j) {
						if (m_teams[i][j]) {
							std::cout << "\t- " << m_teams[i][j]->getName() << std::endl;
							for (auto& it : *m_teams[i][j]->getBag()->getItems()) {
								if (it.second.m_type == "Weapon") {
									std::cout << "\t\t- (" << it.second.m_quantity << ")\t" << m_weapons.at(it.first)->m_type << "\t" << it.first << std::endl;
								}
							}
							for (auto& it : *m_teams[i][j]->getBag()->getItems()) {
								if (it.second.m_type != "Weapon") {
									std::cout << "\t\t- (" << it.second.m_quantity << ")\t" << it.second.m_type << "\t" << it.first << std::endl;
								}
							}
						}
					}
				}
			}
			// If the user had enough money
			if (m_money[player] >= m_shop.at(current_item).m_price) {
				// Chose who gets the item
				std::cout << std::endl << "Dans quel inventaire ira \"" << current_item << "\" ?" <<
					std::endl;
				std::getline(std::cin, user_input);
				// If the user is really rude
				if (user_input == "fuck") {
					err = "On arrete tout, retour au magasin";
					jump = true;
				}
				if (jump) {
					jump = false;
					continue;
				}
				// Check if the character exists
				for (int i = 0; i < 3; ++i) {
					// If he does
					if (m_teams[player][i]->getName() == user_input) {
						// Pay up and register transaction data
						jump = true;
						m_money[player] -= m_shop.at(current_item).m_price;
						last_transaction.character_id = i;
						last_transaction.item_name = current_item;
						last_transaction.price = m_shop.at(current_item).m_price;
						transaction_history->push_back(last_transaction);
						// Check if he already has the item
						if (m_teams[player][i]->getBag()->getItems()->find(current_item) != m_teams[player][i]->getBag()->getItems()->end()) {
							// If he has it
							++m_teams[player][i]->getBag()->getItems()->at(current_item).m_quantity;
						}
						else {
							// If he doesn't
							m_teams[player][i]->getBag()->addItem(current_item, m_shop.at(current_item).m_type, m_shop.at(current_item).m_usable);
						}
					}
				}
				// If he doesn't
				if (jump) {
					jump = false;
					continue;
				}
				err = "Le personnage choisi n'existe pas !";
			}
			else {
				err = "Trop pauvre !";
			}
		}
	}


	// If the game is in multiplayer, exchange shopping data
	last_transaction.character_id = -1;
	last_transaction.item_name = "";
	last_transaction.price = -1;
	if (m_multiplayer) {
		system(clearcommand);
		std::cout << std::endl << "En attente d'un autre joueur" << std::endl;
		sf::Packet packet;
		if (m_server) {
			// Wait for start of transmission
			while (last_transaction.item_name != "SOT") {
				packet << static_cast<int>(-1) << "RDY" << static_cast<int>(-1);
				m_net->getClient().send(packet);
				packet.clear();
				m_net->getClient().receive(packet);
				packet >> last_transaction.character_id >> last_transaction.item_name >> last_transaction.price;
				packet.clear();
			}
			// Receive all packets
			while (last_transaction.item_name != "EOT") {
				last_transaction.character_id = -1;
				last_transaction.item_name = "";
				last_transaction.price = -1;
				while (last_transaction.item_name.empty()) {
					m_net->getClient().receive(packet);
					packet >> last_transaction.character_id >> last_transaction.item_name >> last_transaction.price;
					packet.clear();
				}
				if (last_transaction.item_name == "EOT") {
					break;
				}
				m_money[1] -= last_transaction.price;
				if (m_teams[1][last_transaction.character_id]->getBag()->getItems()->find(last_transaction.item_name) != m_teams[1][last_transaction.character_id]->getBag()->getItems()->end()) {
					++m_teams[1][last_transaction.character_id]->getBag()->getItems()->at(last_transaction.item_name).m_quantity;
				}
				else {
					m_teams[1][last_transaction.character_id]->getBag()->addItem(last_transaction.item_name, m_shop.at(last_transaction.item_name).m_type, m_shop.at(last_transaction.item_name).m_usable);
				}
			}
			// Send all packets
			// Wait for ready
			while (last_transaction.item_name != "RDY") {
				packet << static_cast<int>(-1) << "SOT" << static_cast<int>(-1);
				m_net->getClient().send(packet);
				packet.clear();
				m_net->getClient().receive(packet);
				packet >> last_transaction.character_id >> last_transaction.item_name >> last_transaction.price;
				packet.clear();
			}
			// Send everything
			for (auto it = transaction_history->rbegin(); it != transaction_history->rend(); ++it) {
				if (it->item_name != "done") {
					packet << it->character_id << it->item_name << it->price;
					m_net->getClient().send(packet);
					packet.clear();
				}
			}
			packet << static_cast<int>(-1) << "EOT" << static_cast<int>(-1);
			m_net->getClient().send(packet);
			packet.clear();
		}
		else {
			// Send all packets
			// Wait for ready
			while (last_transaction.item_name != "RDY") {
				packet << static_cast<int>(-1) << "SOT" << static_cast<int>(-1);
				m_net->getSocket().send(packet);
				packet.clear();
				m_net->getSocket().receive(packet);
				packet >> last_transaction.character_id >> last_transaction.item_name >> last_transaction.price;
				packet.clear();
			}
			// Send everything
			for (auto it = transaction_history->rbegin(); it != transaction_history->rend(); ++it) {
				if (it->item_name != "done") {
					packet << it->character_id << it->item_name << it->price;
					m_net->getSocket().send(packet);
					packet.clear();
				}
			}
			packet << static_cast<int>(-1) << "EOT" << static_cast<int>(-1);
			m_net->getSocket().send(packet);
			packet.clear();
			// Wait for start of transmission
			while (last_transaction.item_name != "SOT") {
				packet << static_cast<int>(-1) << "RDY" << static_cast<int>(-1);
				m_net->getSocket().send(packet);
				packet.clear();
				m_net->getSocket().receive(packet);
				packet >> last_transaction.character_id >> last_transaction.item_name >> last_transaction.price;
				packet.clear();
			}
			// Receive all packets
			while (last_transaction.item_name != "EOT") {
				last_transaction.character_id = -1;
				last_transaction.item_name = "";
				last_transaction.price = -1;
				while (last_transaction.item_name.empty()) {
					m_net->getSocket().receive(packet);
					packet >> last_transaction.character_id >> last_transaction.item_name >> last_transaction.price;
					packet.clear();
				}
				if (last_transaction.item_name == "EOT") {
					break;
				}
				m_money[0] -= last_transaction.price;
				if (m_teams[0][last_transaction.character_id]->getBag()->getItems()->find(last_transaction.item_name) != m_teams[0][last_transaction.character_id]->getBag()->getItems()->end()) {
					++m_teams[0][last_transaction.character_id]->getBag()->getItems()->at(last_transaction.item_name).m_quantity;
				}
				else {
					m_teams[0][last_transaction.character_id]->getBag()->addItem(last_transaction.item_name, m_shop.at(last_transaction.item_name).m_type, m_shop.at(last_transaction.item_name).m_usable);
				}
			}
		}
	}

	return success;
}

int Game::equipWeapons()
{
	// User input variable
	std::string user_input;
	// Who's turn it is
	int player = 0;
	bool ready = false;
	// If there was an error in the user input
	std::string err;
	// Item being focussed
	std::string current_item_type;
	// Weapon to declare / equip
	Weapon *weapon;
	// Remember the last transaction details in order to be able to cancel it
	struct s_transaction
	{
		int item_owner_id = -1;
		int weapon_owner_id = -1;
		std::string item_name;
		std::string weapon_name;
	};
	std::vector<struct s_transaction> *transaction_history;
	if (m_transactions.find("equipWeapons") == m_transactions.end()) {
		transaction_history = new std::vector<struct s_transaction>;
		m_transactions.insert(std::make_pair("equipWeapons", transaction_history));
	}
	else {
		transaction_history = reinterpret_cast<std::vector<struct s_transaction>*>(m_transactions.at("equipWeapons"));
		player = 1;
		transaction_history->pop_back();
	}
	struct s_transaction last_transaction;
	// Copy variables
	int istats_tmp[NB_STATS];
	int iwstats_tmp[NB_STATSW];

	// Change the init value according to SP or MP mode
	if (m_multiplayer && !m_server) {
		player = 1;
	}
	// Choose the weapons
	while (!ready) {
		// Clear screen
		system(clearcommand);
		std::cout <<
			// Display possible error
			err << std::endl <<
			// Choosing message
			"Equipez vos soldats ! Au tour du Joueur " << player + 1 <<
			std::endl;
		err = "";
		// Display the current bag contents
		for (int i = 0; i < 2; ++i) {
			std::cout << std::endl << "Joueur " << i + 1 << std::endl;
			for (int j = 0; j < 3; ++j) {
				if (m_teams[i][j]) {
					if (m_teams[i][j]->getWeapon()) {
						std::cout << "\t- " << m_teams[i][j]->getName() << " (" << m_teams[i][j]->getWeapon()->getName() << ")" << std::endl;
					}
					else {
						std::cout << "\t- " << m_teams[i][j]->getName() << std::endl;
					}
					for (auto& it : *m_teams[i][j]->getBag()->getItems()) {
						if (it.second.m_type == "Weapon") {
							std::cout << "\t\t- (" << it.second.m_quantity << ")\t" << m_weapons.at(it.first)->m_type << "\t" << it.first << std::endl;
						}
					}
				}
			}
		}
		std::cout << std::endl << "Entrer le nom de l'arme a equiper" << std::endl;
		err = "";
		int chosen_owner_id = -1;
		int item_count = 0;
		// User input
		std::getline(std::cin, user_input);
		std::string current_item_name = user_input;
		// If the item exists
		for (int i = 0; i < 3; ++i) {
			if (m_teams[player][i]->getBag()->getItems()->find(current_item_name) != m_teams[player][i]->getBag()->getItems()->end()) {
				chosen_owner_id = i;
				++item_count;
			}
		}

		// If the user is really rude
		if (user_input == "fuck" && !transaction_history->empty()) {
			// If the last operation was changing whose turn it is to equip
			if (transaction_history->back().item_name == "done") {
				--player;
				transaction_history->pop_back();
				continue;
			}
			// Check if the old owner already has the item
			if (m_teams[player][transaction_history->back().item_owner_id]->getBag()->getItems()->find(transaction_history->back().item_name) != m_teams[player][transaction_history->back().item_owner_id]->getBag()->getItems()->end()) {
				// If he has it
				++m_teams[player][transaction_history->back().item_owner_id]->getBag()->getItems()->at(transaction_history->back().item_name).m_quantity;
			}
			else {
				// If he doesn't
				m_teams[player][transaction_history->back().item_owner_id]->getBag()->addItem(transaction_history->back().item_name, m_shop.at(transaction_history->back().item_name).m_type, false);
			}
			// Unequip it from the new user
			m_teams[player][transaction_history->back().weapon_owner_id]->unequipWeapon();
			// If it was a swap
			if (!transaction_history->back().weapon_name.empty()) {
				// Equip
				current_item_type = m_weapons.at(transaction_history->back().weapon_name)->m_type;
				for (int i = 0; i < NB_STATS; ++i) {
					istats_tmp[i] = m_weapons.at(transaction_history->back().weapon_name)->m_stats[i];
				}
				for (int i = 0; i < NB_STATSW; ++i) {
					iwstats_tmp[i] = m_weapons.at(transaction_history->back().weapon_name)->m_wstats[i];
				}
				if (current_item_type == "Sword") {
					weapon = static_cast<Sword *>(new Sword(transaction_history->back().weapon_name, istats_tmp, iwstats_tmp));
				}
				else if (current_item_type == "Staff") {
					weapon = static_cast<Staff *>(new Staff(transaction_history->back().weapon_name, istats_tmp, iwstats_tmp));
				}
				else if (current_item_type == "Dagger") {
					weapon = static_cast<Dagger *>(new Dagger(transaction_history->back().weapon_name, istats_tmp, iwstats_tmp));
				}
				else if (current_item_type == "Bow") {
					weapon = static_cast<Bow *>(new Bow(transaction_history->back().weapon_name, istats_tmp, iwstats_tmp));
				}
				else {
					return err_wrongClassFormat;
				}
				m_teams[player][transaction_history->back().weapon_owner_id]->setWeapon(static_cast<Weapon *>(weapon));
				// Check if the old owner has more than one of the item in his bag
				if (m_teams[player][transaction_history->back().item_owner_id]->getBag()->getItems()->at(transaction_history->back().weapon_name).m_quantity > 1) {
					// If he has more
					--m_teams[player][transaction_history->back().item_owner_id]->getBag()->getItems()->at(transaction_history->back().weapon_name).m_quantity;
				}
				else {
					// If he doesn't
					m_teams[player][transaction_history->back().item_owner_id]->getBag()->getItems()->erase(m_teams[player][transaction_history->back().item_owner_id]->getBag()->getItems()->find(transaction_history->back().weapon_name));
				}
			}
			transaction_history->pop_back();
		}
		else if (user_input == "fuck" && transaction_history->empty()) {
			delete transaction_history;
			m_transactions.erase("equipWeapons");
			return fuck;
		}
		// If the user wants to stop equipping
		else if (user_input == "done") {
			last_transaction.item_owner_id = -1;
			last_transaction.weapon_owner_id = -1;
			last_transaction.item_name = "done";
			last_transaction.weapon_name = "";
			transaction_history->push_back(last_transaction);
			if (player == 0 && !m_server) {
				++player;
			}
			else {
				ready = true;
			}
		}
		// If the item doesn't exist
		else if (!item_count) {
			err = "Aucun joueur de votre equipe n'a cet objet !";
		}
		// If the item exists
		else {
			// If multiple characters have the item
			if (item_count > 1) {
				std::cout <<
					"Plusieurs personnages ont cet objet, lequel choisir ?" <<
					std::endl;
				chosen_owner_id = -1;
				while (chosen_owner_id == -1) {
					std::getline(std::cin, user_input);
					if (user_input == "fuck") {
						continue;
					}
					for (int i = 0; i < 3; ++i) {
						if (user_input == m_teams[player][i]->getName()) {
							chosen_owner_id = i;
						}
					}
				}
			}
			// Prompt who will get the item
			std::cout <<
				"Qui sera equipe de cet objet ?" <<
				std::endl;
			int chosen_receiver_id = -1;
			while (chosen_receiver_id == -1) {
				std::getline(std::cin, user_input);
				if (user_input == "fuck") {
					continue;
				}
				for (int i = 0; i < 3; ++i) {
					if (user_input == m_teams[player][i]->getName()) {
						chosen_receiver_id = i;
					}
				}
			}
			// Instanciate the weapon
			current_item_type = m_weapons.at(current_item_name)->m_type;
			for (int i = 0; i < NB_STATS; ++i) {
				istats_tmp[i] = m_weapons.at(current_item_name)->m_stats[i];
			}
			for (int i = 0; i < NB_STATSW; ++i) {
				iwstats_tmp[i] = m_weapons.at(current_item_name)->m_wstats[i];
			}
			if (current_item_type == "Sword") {
				weapon = static_cast<Sword *>(new Sword(current_item_name, istats_tmp, iwstats_tmp));
			}
			else if (current_item_type == "Staff") {
				weapon = static_cast<Staff *>(new Staff(current_item_name, istats_tmp, iwstats_tmp));
			}
			else if (current_item_type == "Dagger") {
				weapon = static_cast<Dagger *>(new Dagger(current_item_name, istats_tmp, iwstats_tmp));
			}
			else if (current_item_type == "Bow") {
				weapon = static_cast<Bow *>(new Bow(current_item_name, istats_tmp, iwstats_tmp));
			}
			else {
				return err_wrongClassFormat;
			}
			std::string current_character_type = m_characters.at(m_teams[player][chosen_receiver_id]->getName())->m_type;
			// If the character can't wield the weapon
			std::set<std::string> compatibility = weapon->getCompatibility();
			if (compatibility.find(current_character_type) == compatibility.end()) {
				err = "Le personnage ne peut pas manier cette arme !";
				// Go back
				delete weapon;
				weapon = nullptr;
				continue;
			}
			last_transaction.item_name = current_item_name;
			last_transaction.item_owner_id = chosen_owner_id;
			last_transaction.weapon_owner_id = chosen_receiver_id;
			// If the character already has a weapon
			if (m_teams[player][chosen_receiver_id]->getWeapon()) {
				last_transaction.weapon_name = m_teams[player][chosen_receiver_id]->getWeapon()->getName();
				// Prompt for swap
				std::cout <<
					"Le personnage manie deja une arme, voulez-vous la mettre dans le sac ? [y/n]" <<
					std::endl;
				while (user_input != "y" && user_input != "n" && user_input != "fuck") {
					std::getline(std::cin, user_input);
				}
				// If the player accepted
				if (user_input == "y") {
					// Unequip his weapon
					// Check if the old owner already has the item
					if (m_teams[player][chosen_owner_id]->getBag()->getItems()->find(m_teams[player][chosen_receiver_id]->getWeapon()->getName()) != m_teams[player][chosen_owner_id]->getBag()->getItems()->end()) {
						// If he has it
						++m_teams[player][chosen_owner_id]->getBag()->getItems()->at(m_teams[player][chosen_receiver_id]->getWeapon()->getName()).m_quantity;
					}
					else {
						// If he doesn't
						m_teams[player][chosen_owner_id]->getBag()->addItem(m_teams[player][chosen_receiver_id]->getWeapon()->getName(), m_shop.at(m_teams[player][chosen_receiver_id]->getWeapon()->getName()).m_type, false);
					}
					// Unequip it from the new user
					m_teams[player][chosen_owner_id]->unequipWeapon();
				}
				// If the player refused
				else if (user_input == "n" || user_input == "fuck") {
					// Go back
					delete weapon;
					weapon = nullptr;
					continue;
				}
			}
			else {
				last_transaction.weapon_name = "";
			}
			// Equip
			m_teams[player][chosen_receiver_id]->setWeapon(static_cast<Weapon *>(weapon));
			// Check if the old owner has more than one of the item in his bag
			if (m_teams[player][chosen_owner_id]->getBag()->getItems()->at(current_item_name).m_quantity > 1) {
				// If he has more
				--m_teams[player][chosen_owner_id]->getBag()->getItems()->at(current_item_name).m_quantity;
			}
			else {
				// If he doesn't
				m_teams[player][chosen_owner_id]->getBag()->getItems()->erase(m_teams[player][chosen_owner_id]->getBag()->getItems()->find(current_item_name));
			}
			transaction_history->push_back(last_transaction);
		}
	}

	// If the game is in multiplayer, exchange shopping data
	last_transaction.item_owner_id = -1;
	last_transaction.item_name = "";
	last_transaction.weapon_owner_id = -1;
	last_transaction.weapon_name = "";
	if (m_multiplayer) {
		system(clearcommand);
		std::cout << std::endl << "En attente d'un autre joueur" << std::endl;
		sf::Packet packet;
		if (m_server) {
			// Wait for start of transmission
			while (last_transaction.item_name != "SOT") {
				packet << static_cast<int>(-1) << "RDY" << static_cast<int>(-1) << "";
				m_net->getClient().send(packet);
				packet.clear();
				m_net->getClient().receive(packet);
				packet >> last_transaction.item_owner_id >> last_transaction.item_name >> last_transaction.weapon_owner_id << last_transaction.weapon_name;
				packet.clear();
			}
			// Receive all packets
			while (last_transaction.item_name != "EOT") {
				last_transaction.item_owner_id = -1;
				last_transaction.item_name = "";
				last_transaction.weapon_owner_id = -1;
				last_transaction.weapon_name = "";
				while (last_transaction.item_name.empty()) {
					m_net->getClient().receive(packet);
					packet >> last_transaction.item_owner_id >> last_transaction.item_name >> last_transaction.weapon_owner_id << last_transaction.weapon_name;
					packet.clear();
				}
				if (last_transaction.item_name == "EOT") {
					break;
				}

				// Instanciate the weapon
				current_item_type = m_weapons.at(last_transaction.item_name)->m_type;
				for (int i = 0; i < NB_STATS; ++i) {
					istats_tmp[i] = m_weapons.at(last_transaction.item_name)->m_stats[i];
				}
				for (int i = 0; i < NB_STATSW; ++i) {
					iwstats_tmp[i] = m_weapons.at(last_transaction.item_name)->m_wstats[i];
				}
				if (current_item_type == "Sword") {
					weapon = static_cast<Sword *>(new Sword(last_transaction.item_name, istats_tmp, iwstats_tmp));
				}
				else if (current_item_type == "Staff") {
					weapon = static_cast<Staff *>(new Staff(last_transaction.item_name, istats_tmp, iwstats_tmp));
				}
				else if (current_item_type == "Dagger") {
					weapon = static_cast<Dagger *>(new Dagger(last_transaction.item_name, istats_tmp, iwstats_tmp));
				}
				else if (current_item_type == "Bow") {
					weapon = static_cast<Bow *>(new Bow(last_transaction.item_name, istats_tmp, iwstats_tmp));
				}
				else {
					return err_wrongClassFormat;
				}
				if (m_teams[1][last_transaction.weapon_owner_id]->getWeapon()) {
					last_transaction.weapon_name = m_teams[1][last_transaction.weapon_owner_id]->getWeapon()->getName();
					// Unequip his weapon
					// Check if the old owner already has the item
					if (m_teams[1][last_transaction.item_owner_id]->getBag()->getItems()->find(m_teams[1][last_transaction.weapon_owner_id]->getWeapon()->getName()) != m_teams[1][last_transaction.item_owner_id]->getBag()->getItems()->end()) {
						// If he has it
						++m_teams[1][last_transaction.item_owner_id]->getBag()->getItems()->at(m_teams[1][last_transaction.weapon_owner_id]->getWeapon()->getName()).m_quantity;
					}
					else {
						// If he doesn't
						m_teams[1][last_transaction.item_owner_id]->getBag()->addItem(m_teams[1][last_transaction.weapon_owner_id]->getWeapon()->getName(), m_shop.at(m_teams[1][last_transaction.weapon_owner_id]->getWeapon()->getName()).m_type, false);
					}
					// Unequip it from the new user
					m_teams[1][last_transaction.item_owner_id]->unequipWeapon();
				}
				else {
					last_transaction.weapon_name = "";
				}
				// Equip
				m_teams[1][last_transaction.weapon_owner_id]->setWeapon(static_cast<Weapon *>(weapon));
				// Check if the old owner has more than one of the item in his bag
				if (m_teams[1][last_transaction.item_owner_id]->getBag()->getItems()->at(last_transaction.item_name).m_quantity > 1) {
					// If he has more
					--m_teams[1][last_transaction.item_owner_id]->getBag()->getItems()->at(last_transaction.item_name).m_quantity;
				}
				else {
					// If he doesn't
					m_teams[1][last_transaction.item_owner_id]->getBag()->getItems()->erase(m_teams[1][last_transaction.item_owner_id]->getBag()->getItems()->find(last_transaction.item_name));
				}
			}
			// Send all packets
			// Wait for ready
			while (last_transaction.item_name != "RDY") {
				packet << static_cast<int>(-1) << "SOT" << static_cast<int>(-1) << "";
				m_net->getClient().send(packet);
				packet.clear();
				m_net->getClient().receive(packet);
				packet >> last_transaction.item_owner_id >> last_transaction.item_name >> last_transaction.weapon_owner_id >> last_transaction.weapon_name;
				packet.clear();
			}
			// Send everything
			for (auto it = transaction_history->rbegin(); it != transaction_history->rend(); ++it) {
				if (it->item_name != "done") {
					packet << it->item_owner_id << it->item_name << it->weapon_owner_id >> it->weapon_name;
					m_net->getClient().send(packet);
					packet.clear();
				}
			}
			packet << static_cast<int>(-1) << "EOT" << static_cast<int>(-1) << "";
			m_net->getClient().send(packet);
			packet.clear();
		}
		else {
			// Send all packets
			// Wait for ready
			while (last_transaction.item_name != "RDY") {
				packet << static_cast<int>(-1) << "SOT" << static_cast<int>(-1) << "";
				m_net->getSocket().send(packet);
				packet.clear();
				m_net->getSocket().receive(packet);
				packet >> last_transaction.item_owner_id >> last_transaction.item_name >> last_transaction.weapon_owner_id >> last_transaction.weapon_name;
				packet.clear();
			}
			// Send everything
			for (auto it = transaction_history->rbegin(); it != transaction_history->rend(); ++it) {
				if (it->item_name != "done") {
					packet << it->item_owner_id << it->item_name << it->weapon_owner_id >> it->weapon_name;
					m_net->getSocket().send(packet);
					packet.clear();
				}
			}
			packet << static_cast<int>(-1) << "EOT" << static_cast<int>(-1) << "";
			m_net->getSocket().send(packet);
			packet.clear();
			// Wait for start of transmission
			while (last_transaction.item_name != "SOT") {
				packet << static_cast<int>(-1) << "RDY" << static_cast<int>(-1) << "";
				m_net->getSocket().send(packet);
				packet.clear();
				m_net->getSocket().receive(packet);
				packet >> last_transaction.item_owner_id >> last_transaction.item_name >> last_transaction.weapon_owner_id << last_transaction.weapon_name;
				packet.clear();
			}
			// Receive all packets
			while (last_transaction.item_name != "EOT") {
				last_transaction.item_owner_id = -1;
				last_transaction.item_name = "";
				last_transaction.weapon_owner_id = -1;
				last_transaction.weapon_name = "";
				while (last_transaction.item_name.empty()) {
					m_net->getSocket().receive(packet);
					packet >> last_transaction.item_owner_id >> last_transaction.item_name >> last_transaction.weapon_owner_id << last_transaction.weapon_name;
					packet.clear();
				}
				if (last_transaction.item_name == "EOT") {
					break;
				}

				// Instanciate the weapon
				current_item_type = m_weapons.at(last_transaction.item_name)->m_type;
				for (int i = 0; i < NB_STATS; ++i) {
					istats_tmp[i] = m_weapons.at(last_transaction.item_name)->m_stats[i];
				}
				for (int i = 0; i < NB_STATSW; ++i) {
					iwstats_tmp[i] = m_weapons.at(last_transaction.item_name)->m_wstats[i];
				}
				if (current_item_type == "Sword") {
					weapon = static_cast<Sword *>(new Sword(last_transaction.item_name, istats_tmp, iwstats_tmp));
				}
				else if (current_item_type == "Staff") {
					weapon = static_cast<Staff *>(new Staff(last_transaction.item_name, istats_tmp, iwstats_tmp));
				}
				else if (current_item_type == "Dagger") {
					weapon = static_cast<Dagger *>(new Dagger(last_transaction.item_name, istats_tmp, iwstats_tmp));
				}
				else if (current_item_type == "Bow") {
					weapon = static_cast<Bow *>(new Bow(last_transaction.item_name, istats_tmp, iwstats_tmp));
				}
				else {
					return err_wrongClassFormat;
				}
				if (m_teams[0][last_transaction.weapon_owner_id]->getWeapon()) {
					last_transaction.weapon_name = m_teams[0][last_transaction.weapon_owner_id]->getWeapon()->getName();
					// Unequip his weapon
					// Check if the old owner already has the item
					if (m_teams[0][last_transaction.item_owner_id]->getBag()->getItems()->find(m_teams[0][last_transaction.weapon_owner_id]->getWeapon()->getName()) != m_teams[0][last_transaction.item_owner_id]->getBag()->getItems()->end()) {
						// If he has it
						++m_teams[0][last_transaction.item_owner_id]->getBag()->getItems()->at(m_teams[0][last_transaction.weapon_owner_id]->getWeapon()->getName()).m_quantity;
					}
					else {
						// If he doesn't
						m_teams[0][last_transaction.item_owner_id]->getBag()->addItem(m_teams[0][last_transaction.weapon_owner_id]->getWeapon()->getName(), m_shop.at(m_teams[0][last_transaction.weapon_owner_id]->getWeapon()->getName()).m_type, false);
					}
					// Unequip it from the new user
					m_teams[0][last_transaction.item_owner_id]->unequipWeapon();
				}
				else {
					last_transaction.weapon_name = "";
				}
				// Equip
				m_teams[0][last_transaction.weapon_owner_id]->setWeapon(static_cast<Weapon *>(weapon));
				// Check if the old owner has more than one of the item in his bag
				if (m_teams[0][last_transaction.item_owner_id]->getBag()->getItems()->at(last_transaction.item_name).m_quantity > 1) {
					// If he has more
					--m_teams[0][last_transaction.item_owner_id]->getBag()->getItems()->at(last_transaction.item_name).m_quantity;
				}
				else {
					// If he doesn't
					m_teams[0][last_transaction.item_owner_id]->getBag()->getItems()->erase(m_teams[0][last_transaction.item_owner_id]->getBag()->getItems()->find(last_transaction.item_name));
				}
			}
		}
	}

	return success;
}

void Game::loadPlayer(std::string const& filename) {
	std::ifstream file;
	std::string line;

	std::string class_tmp;
	std::string name_tmp;

	int stats_tmp[NB_STATS];

	file.open(filename);
	if (!file.is_open()) {
		std::cout << "Error loadPlayer() -> Unable to opening file " << filename << std::endl;
		return;
	}

	while (!file.eof()) {
		getline(file, line);

		if (line == "Character") {
			getline(file, line);
			while (line != "#Character") {
				std::vector<std::string> v_line = stringSplit(line, "\t");
				if (v_line[0] == "Class") {
					class_tmp.assign(v_line[1]);
				}
				else if (v_line[0] == "Name") {
					name_tmp.assign(v_line[1]);
				}
				for (int i = 0; i < NB_STATS; ++i) {
					if (v_line[0] == STAT_STRING[i]) {
						stats_tmp[i] = std::stoi(v_line[1]);
						break;
					}
				}
				getline(file, line);
			}

			m_characters.insert(std::make_pair(name_tmp, new s_Player(name_tmp, class_tmp, stats_tmp)));

			for (int i(0); i < NB_STATS; ++i) {
				stats_tmp[i] = 0;
			}
		}
	}
	file.close();
}

void Game::loadWeapon(std::string const& filename) {
	std::ifstream file;
	std::string line;

	std::string type_tmp;
	std::string name_tmp;

	int stats_tmp[NB_STATS];
	int wstats_tmp[NB_STATSW];

	file.open(filename);
	if (!file.is_open()) {
		std::cout << "Error loadWeapon() -> Unable to opening file " << filename << std::endl;
		return;
	}

	while (!file.eof()) {
		getline(file, line);

		if (line == "Weapon") {
			getline(file, line);
			while (line != "#Weapon") {
				std::vector<std::string> v_line = stringSplit(line, "\t");
				if (v_line[0] == "Type") {
					type_tmp.assign(v_line[1]);
				}
				else if (v_line[0] == "Name") {
					name_tmp.assign(v_line[1]);
				}
				else if (v_line[0] == "Price") {
					m_shop.insert(std::make_pair(name_tmp, Game::s_items(std::stoi(v_line[1]), "Weapon", false)));
				}
				else {
					for (int i = 0; i < NB_STATSW; ++i) {
						if (v_line[0] == STATW_STRING[i]) {
							wstats_tmp[i] = std::stoi(v_line[1]);
							break;
						}
					}
					for (int i = 0; i < NB_STATS; ++i) {
						if (v_line[0] == STAT_STRING[i]) {
							stats_tmp[i] = std::stoi(v_line[1]);
							break;
						}
					}
				}
				getline(file, line);
			}

			m_weapons.insert(std::make_pair(name_tmp, new s_Weapon(name_tmp, type_tmp, stats_tmp, wstats_tmp)));
		}

		for (int i(0); i < NB_STATS; ++i) {
			stats_tmp[i] = 0;
		}
		for (int i(0); i < NB_STATSW; ++i) {
			wstats_tmp[i] = 0;
		}
	}
	file.close();

}

void Game::printCharacters(int mode, const std::string& character)
{
	int * char_stats;
	switch (mode)
	{
		// Full - all
	case 0b00:
		for (auto& it : m_characters) {
			char_stats = it.second->m_stats;
			std::cout <<
				it.second->m_type << "\t:\t" << it.second->m_name <<
				std::endl;
			for (int i = 0; i < NB_STATS; ++i) {
				std::cout <<
					STAT_STRING[i] << (STAT_STRING[i].length() >= 8 ? "\t:\t" : "\t\t:\t") << char_stats[i] <<
					std::endl;
			}
			std::cout << std::endl;
		}
		break;

		// Full - one
	case 0b01:
		char_stats = m_characters.at(character)->m_stats;
		std::cout <<
			m_characters.at(character)->m_type << "\t:\t" << m_characters.at(character)->m_name <<
			std::endl;
		for (int i = 0; i < NB_STATS; ++i) {
			std::string stat_pretty = STAT_STRING[i].substr(STAT_STRING[i].find('_') + 1);
			std::cout <<
				stat_pretty << (stat_pretty.length() >= 8 ? "\t:\t" : "\t\t:\t") << char_stats[i] <<
				std::endl;
		}
		std::cout << std::endl;
		break;

		// Less - all
	case 0b10:
		for (auto& it : m_characters) {
			std::cout <<
				it.second->m_type << "\t:\t" << it.second->m_name <<
				std::endl;
		}
		break;

		// Less - one
	case 0b11:
		std::cout <<
			m_characters.at(character)->m_name << "\t:\t" << m_characters.at(character)->m_name <<
			std::endl;
		break;
	default:
		break;
	}
}

void Game::printWeapons(int mode, const std::string& weapon)
{
	int * weapon_stats[2];
	std::string stat_pretty;
	switch (mode)
	{
		// Full - all
	case 0b00:
		for (auto& it : m_weapons) {
			weapon_stats[0] = it.second->m_stats;
			weapon_stats[1] = it.second->m_wstats;

			std::cout <<
				it.second->m_type << "\t:\t" << it.second->m_name <<
				std::endl <<
				"Influence sur les statistiques du joueur :" <<
				std::endl;
			for (int i = 0; i < NB_STATS; ++i) {
				stat_pretty = STAT_STRING[i].substr(STAT_STRING[i].find('_') + 1);
				std::cout <<
					stat_pretty << (stat_pretty.length() >= 8 ? "\t:\t" : "\t\t:\t") << weapon_stats[0][i] <<
					std::endl;
			}
			std::cout <<
				"Statistiques de l'arme :" <<
				std::endl;
			for (int i = 0; i < NB_STATSW; ++i) {
				stat_pretty = STATW_STRING[i].substr(STATW_STRING[i].find('_') + 1);
				std::cout <<
					stat_pretty << (stat_pretty.length() >= 8 ? "\t:\t" : "\t\t:\t") << weapon_stats[1][i] <<
					std::endl;
			}
			std::cout << std::endl;
		}
		break;

		// Full - one
	case 0b01:
		weapon_stats[0] = m_weapons.at(weapon)->m_stats;
		weapon_stats[1] = m_weapons.at(weapon)->m_wstats;

		std::cout <<
			m_weapons.at(weapon)->m_type << "\t:\t" << m_weapons.at(weapon)->m_name <<
			std::endl <<
			"Influence sur les statistiques du joueur :" <<
			std::endl;
		for (int i = 0; i < NB_STATS; ++i) {
			stat_pretty = STAT_STRING[i].substr(STAT_STRING[i].find('_') + 1);
			std::cout <<
				stat_pretty << (stat_pretty.length() >= 8 ? "\t:\t" : "\t\t:\t") << weapon_stats[0][i] <<
				std::endl;
		}
		std::cout <<
			"Statistiques de l'arme :" <<
			std::endl;
		for (int i = 0; i < NB_STATSW; ++i) {
			stat_pretty = STATW_STRING[i].substr(STATW_STRING[i].find('_') + 1);
			std::cout <<
				stat_pretty << (stat_pretty.length() >= 8 ? "\t:\t" : "\t\t:\t") << weapon_stats[1][i] <<
				std::endl;
		}
		std::cout << std::endl;
		break;

		// Less - all
	case 0b10:
		for (auto& it : m_weapons) {
			std::cout <<
				it.second->m_type << "\t:\t" << it.second->m_name <<
				std::endl;
		}
		break;

		// Less - one
	case 0b11:
		std::cout <<
			m_weapons.at(weapon)->m_type << "\t:\t" << m_weapons.at(weapon)->m_name <<
			std::endl;
		break;
	default:
		break;
	}
}
